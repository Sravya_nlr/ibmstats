var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var uuid = require('node-uuid');
var busboy = require('connect-busboy');
var csv = require('fast-csv');
var unzip = require('unzip');
var cheerio = require('cheerio');

/*
var Db = require('mongodb').Db,
    Server = require('mongodb').Server;
*/

// var fs = require('fs-extra');
var dns = require('dns');


var routes = require('./routes/index');
var users = require('./routes/users');
//var user_settings = require('./routes/user_settings');
//var domains = require('./routes/domains');
var top_domains = require('./routes/fb_stats');
//var LI_stats = require('./routes/LI_stats');
//var LI_stats2 = require('./routes/LI_stats2');
//var GSB = require('./routes/GSB');
var ibm = require('./routes/truem');
var daywise_count = require('./routes/fb_valid');
//var domain_settings = require('./routes/domain_settings');
var daywise = require('./routes/daywise');
//var domainwise = require('./routes/domainwise');
//var settings = require('./routes/settings');
//var content_details = require('./routes/content_details');
//var content_details_wmp = require('./routes/content_details_wmp');
//var content_stats = require('./routes/content_stats');
var list_upload = require('./routes/list_upload');
var db="";
var lidb="";

var monk = require('monk');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(busboy());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/upload', function (req, res) {

    fs.readdir(__dirname + '/public/file/', function (err, files) {

        if (err) {
            res.send(err);
        }
        res.json(files);
    });

});

app.post('/upload', function (req, res, next) {
    var fstream;


    req.pipe(req.busboy);

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log("filename: " + filename);
        //Changing filename to the text entered

        console.log("fieldname: " + fieldname);
        console.log("encoding: " + encoding);
        console.log("mimetype: " + mimetype);

        //Path where file will be uploaded with original filename
        fstream = fs.createWriteStream(__dirname + '/public/file/' + filename);
        file.pipe(fstream);

        fstream.on('close', function () {
            console.log("Upload Finished of " + filename);

            var stream = fs.createReadStream(__dirname + '/public/file/' + filename);
            try {
                function getIsp(e_domain, callback) {
                    dns.resolveMx(e_domain, function (err, result) {
                        if (result != undefined && result != null && result != "") {
                            result.sort(function (a, b) {
                                return parseFloat(b.priority) - parseFloat(a.priority);
                            });
                            var mx = result[0].exchange;
                            if (mx.length > 1) {
                                var mx = mx.split(".");
                                var isp = mx[mx.length - 2].toLowerCase();
                                callback(null, isp);
                            }
                        }
                        else {
                            callback("invalid isp", null);
                        }
                    });
                }

                var collection = db.get("wmp_seedlist");
                var total_mails = 0;
                var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));
                db1.open(function (err, db1) {
                    var cou = 0;
                    var csvStream = csv
                        .parse()
                        .on("data", function (data) {
                            total_mails++;
                            //console.log(total_mails, "total_mails");
                            //console.log(typeof db1);
                            //console.log(++cou, data);
                            //if(db1=='undefined') {
                            //var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));
                            db1.eval('getNextSequenceValue("wmp_seedlist")', function (err, result) {
                                //console.log(total_mails, "total_mails");
                                getIsp(data[0].split('@')[1], function (err, isp) {
                                    if (err) {
                                        console.error(err);
                                    }
                                    else {
                                        collection.insert({_id: result, email: data[0], isp: isp});
                                        //console.log({_id: result, email: data[0]});
                                        total_mails--;
                                        if (total_mails == 0) {
                                            db1.close();
                                            res.redirect('back');
                                        }
                                    }
                                });
                            });
                            //} else {
                            //    db1.open(function (err, db1) {
                            //        db1.eval('getNextSequenceValue("wmp_seedlist")', function (err, result) {
                            //            //console.log(total_mails, "total_mails");
                            //            collection.insert({_id: result, email: data[0]});
                            //            console.log({_id: result, email: data[0]});
                            //            total_mails--;
                            //            if(total_mails==0) {
                            //                //db1.close();
                            //                res.redirect('back');
                            //            }
                            //        });
                            //    });
                            //}
                        })
                        .on("end", function () {
                            console.log("done");
                            //res.redirect('back');
                            //db1.close();
                        });

                    stream.pipe(csvStream);
                });

                //res.redirect('back');           //where to go next
            } catch (err) {
                //if(db1) {
                //    db1.close();
                //}
            }
        });
    });
});
app.get('/uploadZipWmp', function (req, res) {

    fs.readdir(__dirname + '/public/file/', function (err, files) {

        if (err) {
            res.send(err);
        }
        res.json(files);
    });

});

app.get('/uploadZip', function (req, res) {

    fs.readdir(__dirname + '/public/file/', function (err, files) {

        if (err) {
            res.send(err);
        }
        res.json(files);
    });

});

app.post('/uploadZip', function (req, res, next) {
    var fstream;
    req.pipe(req.busboy);

    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log("filename: " + filename);
        //Changing filename to the text entered

        if (filename.indexOf(".zip") > -1 && filename.indexOf('@') > -1) {
            var cat_name = filename.substr(0, filename.indexOf('@'));
            console.log("cat_name: ", cat_name);
            console.log("fieldname: " + fieldname);
            console.log("encoding: " + encoding);
            console.log("mimetype: " + mimetype);

            //Path where file will be uploaded with original filename

            fstream = fs.createWriteStream(__dirname + '/public/file/' + filename);
            file.pipe(fstream);

            fstream.on('close', function () {
                console.log("Upload Finished of " + filename);
                console.log(__dirname + '/public/file/' + filename);

                var exec = require('child_process').exec;
                var cmd = 'rm -rf ' + __dirname + '/public/file/extracted/*';

                exec(cmd, function (error, stdout, stderr) {
                    // command output is in stdout
                    if (error) console.log(error);

                    cmd = 'unzip ' + __dirname + '/public/file/' + filename + ' -d ' + __dirname + '/public/file/extracted/';

                    exec(cmd, function (error, stdout, stderr) {
                        //fs.createReadStream(__dirname + '/public/file/' + filename).pipe(unzip.Extract({ path: __dirname + '/public/file/extracted/' }));
                        var folder = __dirname + "/public/file/extracted/" + filename.replace(".zip", "") + "/";
                        console.log(folder);

                        var path = require('path');

                        function getDirectories(srcpath) {
                            return fs.readdirSync(srcpath).filter(function (file) {
                                return fs.statSync(path.join(srcpath, file)).isDirectory();
                            });
                        }

                        function getFiles(srcpath) {
                            return fs.readdirSync(srcpath).filter(function (file) {
                                return fs.statSync(path.join(srcpath, file)).isFile();
                            });
                        }

                        var images_folder = getDirectories(folder);
                        var inner_folder = folder + images_folder;
                        files = getFiles(inner_folder);
                        var timestamp = (Date.now() / 1000 | 0);
                        var images_ori = {};
                        for (var a in files) {
                            //cmd = 'cp '+inner_folder + "/" + files[a] + ' /home/img/';
                            //exec(cmd, function(error, stdout, stderr) {
                            //    if(error) console.log(error);
                            //});
                            cmd = 'mv ' + inner_folder + "/" + files[a] + ' ' + inner_folder + "/" + (timestamp + files[a]);
                            console.log(cmd);
                            images_ori[files[a]] = timestamp + files[a];
                            exec(cmd, function (error, stdout, stderr) {
                                if (error) console.log(error);
                                else {
                                    cmd = 'cp ' + inner_folder + "/*" + ' /home/img/';
                                    exec(cmd, function (error, stdout, stderr) {
                                        if (error) console.log(error);
                                    });
                                }
                            });
                        }
                        var files = getFiles(folder);
                        for (var a in files) {
                            //console.log("folder + files[a] ", folder + files[a]);
                            fs.readFile(folder + files[a], 'utf8', function (err, data) {
                                if (err) {
                                    return console.log(err);
                                }

                                data = data.trim().replace(/>\s+</g, "><");
                                //data = data.trim().replace(/>\s+</g, "><").replace(new RegExp(images_folder, 'g'), 'http://img.007_DOMAIN_NAME_007');

                                $ = cheerio.load(data);
                                var list = [];
                                $('img').each(function (index, element) {
                                    list.push($(element).attr('src'));
                                });
                                var temp_list = list.slice(0);

                                var len = list.length;

                                for (var a = 0; a < len; a++) {
                                    list[a] = list[a].replace(new RegExp(images_folder, 'g'), 'http://img.007_DOMAIN_NAME_007');
                                }

                                for (var a = 0; a < len; a++) {
                                    data = data.replace(temp_list[a], list[a]);
                                }

                                //console.log(data);
                                for (var a in images_ori) {
                                    data = data.replace(a, images_ori[a]);
                                }
                                //console.log(data);


                                var newContent = {
                                    'category': cat_name,
                                    'content': data
                                };
                                addContent(newContent);

                                function addContent(body) {
                                    var unsub_flag = true;
                                    var collection = db.get('cmp_categories');
                                    var collection2 = db.get('cmp_links');

                                    var category = body['category'];
                                    var content = body['content'];
                                    var new_content = content;
                                    $ = cheerio.load(content);
                                    var list = [];
                                    $('a').each(function (index, element) {
                                        list.push($(element).attr('href'));
                                    });
                                    var temp_list = list.slice(0);

                                    var len = list.length;
                                    var temp_len = len;
                                    if (len === 0) {
                                        after_process();
                                    }
                                    for (var i = 0; i < len; i++) {
                                        if (list[i].indexOf('UNSUB_00700=0') > 0) {
                                            list[i] = "TLD_HERE_000700/unsub?lnk_id=-1&UNSUB_PARAMS_00700";
                                            temp_len--;
                                            unsub_flag = false;
                                            after_process();
                                        } else if (list[i].indexOf('UNSUB_00700=1') > 0) {
                                            unsub_func(i);
                                            function unsub_func(i) {
                                                unsub_flag = false;
                                                collection2.findOne({url: list[i]}, function (err, obj) {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                    if (obj) {
                                                        list[i] = "TLD_HERE_000700/unsub?lnk_id=" + obj._id + "&UNSUB_PARAMS_00700";
                                                        temp_len--;
                                                        after_process();
                                                    } else {

                                                        var Db = require('mongodb').Db,
                                                            Server = require('mongodb').Server;
                                                        var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                                                        db1.open(function (err, db1) {
                                                            db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                                                if (err) {
                                                                    console.error(err);
                                                                }
                                                                list[i] = "TLD_HERE_000700/unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                                                                db1.close();
                                                                collection2.insert({
                                                                    _id: result,
                                                                    url: temp_list[i]
                                                                }, function (err, result) {
                                                                    if (err) {
                                                                        console.error(err);
                                                                    }
                                                                    temp_len--;
                                                                    after_process();
                                                                });
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        } else {
                                            click_func(i);
                                            function click_func(i) {
                                                collection2.findOne({url: list[i]}, function (err, obj) {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                    if (obj) {
                                                        list[i] = "TLD_HERE_000700/click?lnk_id=" + obj._id + "&CLICK_PARAMS_00700";
                                                        temp_len--;
                                                        after_process();
                                                    } else {

                                                        var Db = require('mongodb').Db,
                                                            Server = require('mongodb').Server;
                                                        var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                                                        db1.open(function (err, db1) {
                                                            db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                                                if (err) {
                                                                    console.error(err);
                                                                }
                                                                list[i] = "TLD_HERE_000700/click?lnk_id=" + result + "&CLICK_PARAMS_00700";
                                                                db1.close();
                                                                collection2.insert({
                                                                    _id: result,
                                                                    url: temp_list[i]
                                                                }, function (err, result) {
                                                                    if (err) {
                                                                        console.error(err);
                                                                    }
                                                                    temp_len--;
                                                                    after_process();
                                                                });
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    }

                                    function after_process() {
                                        if (unsub_flag) {
                                            var unsub_cont = '<table width="600px" align="center"><tr><td style="text-align: center; padding:5px 0px;color:rgb(153,153,153);font-size:10px;line-height:1.5em;border:1px solid #fff">To stop receiving these emails please <a href="TLD_HERE_000700/unsub?lnk_id=-1&UNSUB_PARAMS_00700" style="color:rgb(153,153,153)" target="_blank">click here </a> to unsubscribe. <br></td></tr></table></body>';
                                            new_content = new_content.replace('</body>', unsub_cont);
                                            unsub_flag = false;
                                        }
                                        if (temp_len === 0) {
                                            for (var i in list) {
                                                new_content = new_content.replace(temp_list[i], list[i]);
                                            }
                                            var set_doc = {};
                                            set_doc["content"] = content;
                                            set_doc["new_content"] = new_content;
                                            collection.update({"category": new RegExp(category)}, {"$set": set_doc}, {multi: true}, function (err, result) {
                                                if (err) {
                                                    console.error(err);
                                                }
                                                res.send(
                                                    (err === null) ? {msg: ''} : {msg: err}
                                                );
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                });
            });
        } else {
            console.log("no");
        }
    });
});

app.post('/uploadZipWmp', function (req, res, next) {
    var fstream;
    req.pipe(req.busboy);
    var set_doc = {};
    var domain_name;
    req.busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated) {
        console.log('Field [' + fieldname + ']: value: ' + val);
        if (fieldname == 'domain_name') {
            domain_name = val;
        } else {
            set_doc["wmp_content." + fieldname] = val;
        }
    });
    req.busboy.on('finish', function () {
        console.log('Done parsing form!');
        console.log(set_doc);
        //res.writeHead(303, { Connection: 'close', Location: '/content_details_wmp' });
        //res.end();
    });
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        console.log("filename: " + filename);
        //Changing filename to the text entered

        if (filename.indexOf(".zip") > -1) {
            //domain_name = filename.substr(0, filename.indexOf('@'));
            console.log("domain name: ", domain_name);
            console.log("fieldname: " + fieldname);
            console.log("encoding: " + encoding);
            console.log("mimetype: " + mimetype);

            //Path where file will be uploaded with original filename

            fstream = fs.createWriteStream(__dirname + '/public/file/' + filename);
            file.pipe(fstream);

            fstream.on('close', function () {
                console.log("Upload Finished of " + filename);
                console.log(__dirname + '/public/file/' + filename);

                var exec = require('child_process').exec;
                var cmd = 'rm -rf ' + __dirname + '/public/file/extracted/*';

                exec(cmd, function (error, stdout, stderr) {
                    // command output is in stdout
                    if (error) console.log(error);

                    cmd = 'unzip ' + __dirname + '/public/file/' + filename + ' -d ' + __dirname + '/public/file/extracted/';

                    exec(cmd, function (error, stdout, stderr) {
                        if (error) console.log(error);
                        //fs.createReadStream(__dirname + '/public/file/' + filename).pipe(unzip.Extract({ path: __dirname + '/public/file/extracted/' }));
                        var folder = __dirname + "/public/file/extracted/" + filename.replace(".zip", "") + "/";
                        console.log(folder);

                        var path = require('path');

                        function getDirectories(srcpath) {
                            return fs.readdirSync(srcpath).filter(function (file) {
                                return fs.statSync(path.join(srcpath, file)).isDirectory();
                            });
                        }

                        function getFiles(srcpath) {
                            return fs.readdirSync(srcpath).filter(function (file) {
                                return fs.statSync(path.join(srcpath, file)).isFile();
                            });
                        }

                        var images_folder = getDirectories(folder);
                        var inner_folder = folder + images_folder;
                        files = getFiles(inner_folder);
                        var timestamp = (Date.now() / 1000 | 0);
                        var images_ori = {};
                        for (var a in files) {
                            cmd = 'mv ' + inner_folder + "/" + files[a] + ' ' + inner_folder + "/" + (timestamp + files[a]);
                            console.log(cmd);
                            images_ori[files[a]] = timestamp + files[a];
                            exec(cmd, function (error, stdout, stderr) {
                                if (error) console.log(error);
                                else {
                                    cmd = 'cp ' + inner_folder + "/*" + ' /home/img/';
                                    exec(cmd, function (error, stdout, stderr) {
                                        if (error) console.log(error);
                                    });
                                }
                            });
                        }
                        var files = getFiles(folder);
                        for (var a in files) {
                            //console.log("folder + files[a] ", folder + files[a]);
                            fs.readFile(folder + files[a], 'utf8', function (err, data) {
                                if (err) {
                                    return console.log(err);
                                }

                                //data = data.trim().replace(/>\s+</g, "><").replace(new RegExp(images_folder, 'g'), 'http://img.007_DOMAIN_NAME_007');
                                data = data.trim().replace(/>\s+</g, "><");
                                $ = cheerio.load(data);
                                var list = [];
                                $('img').each(function (index, element) {
                                    list.push($(element).attr('src'));
                                });
                                var temp_list = list.slice(0);

                                var len = list.length;

                                for (var a = 0; a < len; a++) {
                                    list[a] = list[a].replace(new RegExp(images_folder, 'g'), 'http://img.007_DOMAIN_NAME_007');
                                }

                                //console.log("img.list "+list);
                                for (var a = 0; a < len; a++) {
                                    data = data.replace(temp_list[a], list[a]);
                                }
                                //console.log("img.data "+data);
                                for (var a in images_ori) {
                                    data = data.replace(a, images_ori[a]);
                                }

                                var newContent = {
                                    'category': domain_name,
                                    'content': data
                                };
                                addContent(newContent);

                                function addContent(body) {
                                    var unsub_flag = true;
                                    var collection = db.get('cmp_categories');
                                    var collection2 = db.get('cmp_links');

                                    var category = body['category'];
                                    var content = body['content'];
                                    content = content.replace(/&amp;/g, '&');
                                    var new_content = content;
                                    $ = cheerio.load(content);
                                    var list = [];
                                    $('a').each(function (index, element) {
                                        list.push($(element).attr('href'));
                                    });
                                    var temp_list = list.slice(0);

                                    //console.log("temp_list", temp_list);

                                    var len = list.length;
                                    var temp_len = len;
                                    if (len === 0) {
                                        after_process();
                                    }
                                    for (var i = 0; i < len; i++) {
                                        if (list[i].indexOf('UNSUB_00700=0') > 0) {
                                            list[i] = "TLD_HERE_000700/wmp_unsub?lnk_id=-1&UNSUB_PARAMS_00700";
                                            temp_len--;
                                            unsub_flag = false;
                                            after_process();
                                        } else if (list[i].indexOf('UNSUB_00700=1') > 0) {
                                            unsub_func(i);
                                            function unsub_func(i) {
                                                unsub_flag = false;
                                                collection2.findOne({url: list[i]}, function (err, obj) {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                    if (obj) {
                                                        list[i] = "TLD_HERE_000700/wmp_unsub?lnk_id=" + obj._id + "&UNSUB_PARAMS_00700";
                                                        temp_len--;
                                                        after_process();
                                                    } else {

                                                        var Db = require('mongodb').Db,
                                                            Server = require('mongodb').Server;
                                                        var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                                                        db1.open(function (err, db1) {
                                                            db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                                                if (err) {
                                                                    console.error(err);
                                                                }
                                                                list[i] = "TLD_HERE_000700/wmp_unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                                                                db1.close();
                                                                collection2.insert({
                                                                    _id: result,
                                                                    url: temp_list[i]
                                                                }, function (err, result) {
                                                                    if (err) {
                                                                        console.error(err);
                                                                    }
                                                                    temp_len--;
                                                                    after_process();
                                                                });
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        } else {
                                            click_func(i);
                                            function click_func(i) {
                                                collection2.findOne({url: list[i]}, function (err, obj) {
                                                    if (err) {
                                                        console.error(err);
                                                    }
                                                    if (obj) {
                                                        list[i] = "TLD_HERE_000700/wmp_click?lnk_id=" + obj._id + "&CLICK_PARAMS_00700";
                                                        temp_len--;
                                                        after_process();
                                                    } else {
                                                        var Db = require('mongodb').Db,
                                                            Server = require('mongodb').Server;
                                                        var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                                                        db1.open(function (err, db1) {
                                                            db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                                                if (err) {
                                                                    console.error(err);
                                                                }
                                                                list[i] = "TLD_HERE_000700/wmp_click?lnk_id=" + result + "&CLICK_PARAMS_00700";
                                                                db1.close();
                                                                collection2.insert({
                                                                    _id: result,
                                                                    url: temp_list[i]
                                                                }, function (err, result) {
                                                                    if (err) {
                                                                        console.error(err);
                                                                    }
                                                                    temp_len--;
                                                                    after_process();
                                                                });
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    function after_process() {
                                        console.log("completed...");
                                        if (unsub_flag) {
                                            var unsub_cont = '<table width="600px" align="center"><tr><td style="text-align: center; padding:5px 0px;color:rgb(153,153,153);font-size:10px;line-height:1.5em;border:1px solid #fff">To stop receiving these emails please <a href="TLD_HERE_000700/wmp_unsub?lnk_id=-1&UNSUB_PARAMS_00700" style="color:rgb(153,153,153)" target="_blank">click here </a> to unsubscribe. <br></td></tr></table></body>';
                                            new_content = new_content.replace('</body>', unsub_cont);
                                            unsub_flag = false;
                                        }
                                        if (temp_len === 0) {
                                            for (var i in list) {
                                                new_content = new_content.replace(temp_list[i], list[i]);
                                            }
                                            //console.log({"domain_name" : new RegExp(domain_name)});
                                            //db.get('domain_master').find({"domain_name" : new RegExp(domain_name)}, function(err, res) {console.log(res.length);});
                                            set_doc["wmp_content.content"] = content;
                                            set_doc["wmp_content.body"] = new_content;
                                            //console.log({"domain_name" : domain_name},{"$set":set_doc});
                                            console.log(set_doc, domain_name);
                                            db.get('domain_master').update({"domain_name": new RegExp(domain_name)}, {"$set": set_doc}, {multi: true}, function (err, result) {
                                                if (err) {
                                                    console.error(err);
                                                }
                                                //res.send(
                                                //    (err === null) ? {msg: ''} : {msg: err}
                                                //);
                                                res.writeHead(303, {
                                                    Connection: 'close',
                                                    Location: '/content_details_wmp'
                                                });
                                                res.end();
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                });
            });
        } else {
            console.log("no");
        }
    });
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {

    req.uuid = uuid;
    next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/top_domains', top_domains);
//app.use('/LI_stats', LI_stats);
//app.use('/LI_stats2', LI_stats2);
//app.use('/GSB', GSB);
app.use('/ibm', ibm);
app.use('/daywise_count', daywise_count);

//app.use('/user_settings', user_settings);
//app.use('/domains', domains);
//app.use('/domain_settings', domain_settings);
//app.use('/daywise', daywise);
//app.use('/domainwise', domainwise);
//app.use('/settings', settings);
//app.use('/content_details', content_details);
//app.use('/content_details_wmp', content_details_wmp);
//app.use('/content_stats', content_stats);
//app.use('/list_upload', list_upload);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
