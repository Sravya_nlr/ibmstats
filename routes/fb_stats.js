/**
 * Created by sys050 on 30/3/17.
 */
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var conf = require("./conf");
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('top_domains', {title: 'FB Details - Email Validation Stats'});
});
/*
 * GET domainlist.
 */
function getTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var hour = date.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var minute = date.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }
    var dt = {
        minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
        dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
    };
    return dt;
}
var db = "";
var ibmurl = conf.ibmurl;
MongoClient.connect(ibmurl, function (err, db1) {
    if (err) {
        console.log("Errorr::", err);
    }
    else {
        db = db1;
        router.get('/dom_data', function (req, res) {
            //console.log("Successfully authenticated to database");
            var monthNames = [
                "Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul",
                "Aug", "Sept", "Oct",
                "Nov", "Dec"
            ];
            var day = "";
            var date = (getTime().dateStamp);
            console.log(date);
            var collection = db.collection('dom_dayStats');
            try {
                collection.find({Date: date}, {
                    processed: 1,
                    count: 1,
                    D_id: 1
                }).sort({count: -1}).limit(10).toArray(function (err, items) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        var d_det = [];
                        var i=0;
                        inc(i,date);
                        console.log("responseeeee from daywise_stats", items);
                        function inc(i,date){
                            top(items[i],date,function(err,respp){
                                if(err){
                                    throw err;
                                }
                                else{
                                    i++;
                                    if(i<(items.length)){
                                        inc(i,date);
                                    }
                                    else{
                                        console.log(respp);
                                        res.send(respp);
                                    }
                                }
                            });
                        }
                        function top(dt,date, cb) {
                            db.collection("domain_master").findOne({_id: dt.D_id}, function (err, resp) {
                                if (err) {
                                    throw err;
                                }
                                else {
                                    db.collection("ext1_data").count({
                                        d_id: dt.D_id, created_dt: date,
                                        "$or": [{label: 3}, {label: 1}, {label: 2}, {label: 4}, {label: 5}, {label: 6}, {label: 8}, {label: 9}]
                                    }, function (err, res) {
                                        if (err) {
                                            throw err;
                                        }
                                        else {
                                            db.collection("ext1_data").count({
                                                d_id: dt.D_id, created_dt: date, label: 7
                                            }, function (err, res1) {
                                                if (err) {
                                                    throw err;
                                                }
                                                else {
                                                    //console.log({domain: resp.D_name, inbox: res, spam: res1});
                                                    d_det.push({domain: resp.D_name,D_id:dt.D_id, inbox: res, spam: res1,volume:(res+res1),inbp:((res/(res+res1))*100).toFixed(2),spp:((res1/(res+res1))*100).toFixed(2)});
                                                    cb(null,d_det);
                                                }
                                            });
                                        }

                                    })
                                }
                            })

                        }
                    }
                });

                /*var d1 = (dt._id) * 1000;
                 var date3 = new Date(d1);
                 // console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy",dt._id+'000'.getMonth());
                 day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                 console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy", day)
                 dt._id = day;*/

            }
            catch(errrrrrrrrrrrrrrrrrrrrrrrrrrr)
            {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }
            //db1.close();
            //res.send(items);
        });
    }
});
/*
 * POST to adddomain.
 */
/*router.post('/addDomain', function (req, res) {
 var db = req.db;
 var uuid = req.uuid;
 var collection = db.get('domain_master');
 var body = req.body;
 body["api_key"] = uuid.v1();
 body["priority"] = parseInt((body["priority"]));
 var insert = {};
 var Db = require('mongodb').Db,
 Server = require('mongodb').Server;
 var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

 db1.open(function(err, db1) {
 db1.eval('getNextSequenceValue("cmp_domain_master")', function (err, result) {
 if (err) {
 console.error(err);
 }
 insert["_id"] = result;

 insert["domain_name"] = body["domain_name"];
 insert["domainip"] = body["domain_ip"];
 insert["rbmq_username"] = "test1";
 insert["rbmq_password"] = "test1";
 insert["perday_capacity"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 var one_day_in_ms = 86400000;
 insert["sleep_eachmail"] = {
 google: insert["perday_capacity"].google!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].google) : 0,
 yahoodns: insert["perday_capacity"].yahoodns!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].yahoodns) : 0,
 hotmail: insert["perday_capacity"].hotmail!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].hotmail) : 0,
 aol: insert["perday_capacity"].aol!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].aol) : 0,
 others: insert["perday_capacity"].others!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].others) : 0
 };
 insert["remain_today"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 insert["status"] = {
 google: "inactive",
 yahoodns: "inactive",
 hotmail: "inactive",
 aol: "inactive",
 others: "inactive"
 };
 insert["start_hour"] = {
 google: 0,
 yahoodns: 0,
 hotmail: 0,
 aol: 0,
 others: 0
 };
 insert['mtastats_api_port'] = parseInt(body["api_port"]);

 collection.insert(insert, function (err, result) {
 if (err !== null) {
 console.log("err : " + err);
 }
 res.send(
 (err === null) ? {msg: ''} : {msg: err}
 );
 });
 });
 });
 });*/

/*
 * DELETE to deletedomain.
 */
/*router.delete('/deleteDomain/:id', function (req, res) {
 var db = req.db;
 var collection = db.get('domain_master');
 var userToDelete = req.params.id;
 collection.remove({'_id': parseInt(userToDelete)}, function (err) {
 res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
 });
 });*/

module.exports = router;
