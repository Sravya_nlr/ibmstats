var express = require('express');
var router = express.Router();

/*
 * GET domainlist.
 */
router.get('/oauthStatus', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_conf');
    collection.findOne({}, {yql_cookies_expired: 1}, function (e, doc) {
        res.send({status: doc.yql_cookies_expired});
    });
});

/* POST settings page. */
router.post('/changeOauth', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_conf');
    var credentials = req.body;
    if (credentials['user-agent'] !== "" && credentials['crumb'] !== "" && credentials['cookie'] !== "") {
        collection.update({}, {
            $set: {
                user_agent: credentials['user-agent'],
                crumb: credentials['crumb'],
                cookies: credentials['cookie'],
                yql_cookies_expired: "no"
            }
        }, function (err, result) {
            if (err !== null) {
                console.log("err : " + err);
            }
            res.send(
                (err === null) ? {msg: ''} : {msg: err}
            );
        });
    }
});

module.exports = router;