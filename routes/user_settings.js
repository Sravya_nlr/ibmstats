var express = require('express');
var router = express.Router();

/* GET settings page. */
router.get('/', function (req, res, next) {
    res.render('user_settings', {title: 'User Settings - Email Validation Stats'});
});
/*
 * GET userlist.
 */
router.get('/userlist', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_current_clients');
    //console.log(collection);
    collection.find({}, {}, function (e, docs) {
        res.json(docs);
    });
});
/*
 * POST to adduser.
 */
router.post('/saveuser/:id', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_current_clients');
    var body = req.body;
    var userToSave = req.params.id;
    body["priority"] = parseInt((body["priority"]));
    //console.log(body);
    collection.update({'_id': parseInt(userToSave)}, {$set: body}, function (err, result) {
        if (err !== null) {
            console.log("err : " + err);
        }
        res.send(
            (err === null) ? {msg: ''} : {msg: err}
        );
    });
});

module.exports = router;