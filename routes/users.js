var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('users', {title: 'User Details - Email Validation Stats'});
});
/*
 * GET userlist.
 */
router.get('/userlist', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_current_clients');
    collection.find({}, {}, function (e, docs) {
        res.json(docs);
    });
});
/*
 * POST to adduser.
 */
router.post('/adduser', function (req, res) {
    var db = req.db;
    var uuid = req.uuid;
    var collection = db.get('emv_current_clients');
    //var body = JSON.parse(JSON.stringify(req.body));
    var body = req.body;
    body["api_key"] = uuid.v1();
    body["priority"] = parseInt((body["priority"]));

    var Db = require('mongodb').Db,
        Server = require('mongodb').Server;
    var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

    db1.open(function(err, db1) {
        db1.eval('getNextSequenceValue("cmp_emv_current_clients")', function (err, result) {
            if (err) {
                console.error(err);
            }
            body["_id"] = result;
            db1.close();
            collection.insert(body, function (err, result) {
                if (err !== null) {
                    console.log("err : " + err);
                }
                res.send(
                    (err === null) ? {msg: ''} : {msg: err}
                );
            });
        });
    });
});

/*
 * DELETE the user.
 */
router.delete('/deleteuser/:id', function (req, res) {
    try {
        var db = req.db;
        var collection = db.get('emv_current_clients');
        var userToDelete = req.params.id;
        collection.remove({'_id': parseInt(userToDelete)}, function (err) {
            res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
        });
    } catch(errrr) {
        console.error(errrr);
    }
});

module.exports = router;