var express = require('express');
var router = express.Router();
var cheerio = require('cheerio');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('list_upload', {title: 'Seed list upload - Email Validation Stats'});
});
/*
 * GET userlist.
 */
router.get('/contentlist', function (req, res) {
    var db = req.db;
    //var MongoClient = require('mongodb').MongoClient;
    //MongoClient.connect('mongodb://199.193.113.72:27017/emailcrawl_internal', function(err, db) {
    var collection = db.get('cmp_categories');
    collection.find({},{},function (e, docs) {
        res.json(docs);
    });
    //});
});

/*
 * POST to addcontent.
 */
router.post('/addcontent', function (req, res) {
    var db = req.db;
    var collection = db.get('cmp_categories');
    var collection2 = db.get('cmp_links');

    var body = req.body;
    var category = body['category'];
    var content = body['content'];
    var subject = body['subject'];
    var cat_id = parseInt(body['_id']);
    var new_content = content;
    $ = cheerio.load(content);
    var list = [];
    $('a').each(function (index, element) {
        list.push($(element).attr('href'));
    });
    var temp_list = list.slice(0);
    var unsub_flag = true;

    var len = list.length;
    var temp_len=len;
    if(len===0) {
        after_process();
    }
    for(var i = 0; i<len; i++) {
         if(list[i].indexOf('UNSUB_00700=0')>0) {
             list[i] = "TLD_HERE_000700/unsub?lnk_id=-1&UNSUB_PARAMS_00700";
             temp_len--;
             unsub_flag = false;
             after_process();
         } else if(list[i].indexOf('UNSUB_00700=1')>0) {
             unsub_func(i);
             function unsub_func(i) {
                 unsub_flag = false;
                 collection2.findOne({url:list[i]}, function(err, obj) {
                     if(err) { console.error(err); }
                     if(obj) {
                         list[i] = "TLD_HERE_000700/unsub?lnk_id=" +obj._id+ "&UNSUB_PARAMS_00700";
                         temp_len--;
                         after_process();
                     } else {
                         //var MongoClient = require('mongodb').MongoClient;
                         //MongoClient.connect('mongodb://199.193.113.72:27017/emailcrawl_internal', function(err, db1) {
                         //    //db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                         //    //    if (err) {
                         //    //        console.error(err);
                         //    //    }
                         //    //    list[i] = "TLD_HERE_000700/unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                         //    //    //collection2.insert({_id: result, url: list[i]}, function (err, result) {
                         //    //    //    if (err) {
                         //    //    //        console.error(err);
                         //    //    //    }
                         //    //    //    temp_len--;
                         //    //    //    after_process();
                         //    //    //});
                         //    //});
                         //
                         //    var counters = db1.collection('counters');
                         //    //counters.findAndModify({
                         //    //    query: {_id: 'cmp_links_table'},
                         //    //    update: {$inc: {sequence_value: 1}},
                         //    //    'new': true
                         //    //}, {}, function (err, result) {
                         //    counters.findAndModify(
                         //        {_id: 'cmp_links_table'}, {},
                         //        {$inc: {sequence_value: 1}},
                         //        {'new': true},
                         //        function (err, result) {
                         //            if (err) {
                         //                console.error(err);
                         //            }
                         //            //list[i] = "TLD_HERE_000700/unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                         //            //collection2.insert({_id: result, url: list[i]}, function (err, result) {
                         //            //    if (err) {
                         //            //        console.error(err);
                         //            //    }
                         //            //    temp_len--;
                         //            //    after_process();
                         //            //});
                         //        });
                         //});

                         var Db = require('mongodb').Db,
                             Server = require('mongodb').Server;
                         var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                         db1.open(function(err, db1) {
                             db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                 if (err) {
                                     console.error(err);
                                 }
                                 list[i] = "TLD_HERE_000700/unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                                 db1.close();
                                 collection2.insert({_id: result, url: temp_list[i]}, function (err, result) {
                                     if (err) {
                                         console.error(err);
                                     }
                                     temp_len--;
                                     after_process();
                                 });
                             });
                         });

                         //counters.findAndModify({
                         //    query: {_id: 'cmp_links_table'},
                         //    update: {$inc: {sequence_value: 1}},
                         //    'new': true
                         //}, {}, function (err, result) {
                         //    if (err) {
                         //        console.error(err);
                         //    }
                         //    console.log(result, "f;ljfiojfi");
                         //    //list[i] = "TLD_HERE_000700/unsub?lnk_id=" + result + "&UNSUB_PARAMS_00700";
                         //    //collection2.insert({_id: result, url: list[i]}, function (err, result) {
                         //    //    if (err) {
                         //    //        console.error(err);
                         //    //    }
                         //    //    temp_len--;
                         //    //    after_process();
                         //    //});
                         //});
                     }
                 });
             }
         } else {
             click_func(i);
             function click_func(i) {
                 collection2.findOne({url: list[i]}, function (err, obj) {
                     if (err) {
                         console.error(err);
                     }
                     if (obj) {
                         list[i] = "TLD_HERE_000700/click?lnk_id=" + obj._id + "&CLICK_PARAMS_00700";
                         temp_len--;
                         after_process();
                     } else {

                         var Db = require('mongodb').Db,
                             Server = require('mongodb').Server;
                         var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                         db1.open(function (err, db1) {
                             db1.eval('getNextSequenceValue("cmp_links_table")', function (err, result) {
                                 if (err) {
                                     console.error(err);
                                 }
                                 list[i] = "TLD_HERE_000700/click?lnk_id=" + result + "&CLICK_PARAMS_00700";
                                 db1.close();
                                 collection2.insert({_id: result, url: temp_list[i]}, function (err, result) {
                                     if (err) {
                                         console.error(err);
                                     }
                                     temp_len--;
                                     after_process();
                                 });
                             });
                         });
                     }
                 });
             }
         }
    }

    function after_process() {
        if(unsub_flag) {
            var unsub_cont = '<table width="600px" align="center"><tr><td style="text-align: center; padding:5px 0px;color:rgb(153,153,153);font-size:10px;line-height:1.5em;border:1px solid #fff">To stop receiving these emails please <a href="TLD_HERE_000700/unsub?lnk_id=-1&UNSUB_PARAMS_00700" style="color:rgb(153,153,153)" target="_blank">click here </a> to unsubscribe. <br></td></tr></table></body>';
            new_content = new_content.replace('</body>', unsub_cont);
            unsub_flag = false;
        }
        if(temp_len===0) {
            console.log(list);
            for(var i in list) {
                new_content = new_content.replace(temp_list[i], list[i]);
            }

            collection.findOne({"category": category}, function(err, object) {
                if(object) {
                    collection.update({"category" : category},{"$set":{"subject": subject, "content": content, "new_content": new_content}}, function(err, result) {
                        if(err) { console.error(err);}
                        res.send(
                            (err === null) ? {msg: ''} : {msg: err}
                        );
                    });
                } else {
                    var Db = require('mongodb').Db,
                        Server = require('mongodb').Server;
                    var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

                    db1.open(function (err, db1) {
                        db1.eval('getNextSequenceValue("cmp_categories_table")', function (err, result) {
                            if (err) {
                                console.error(err);
                            }
                            collection.insert({
                                "_id": cat_id,
                                "subject": subject,
                                "category": category,
                                "content": content,
                                "new_content": new_content
                            }, function (err, result) {
                                if (err !== null) {
                                    console.log("err : " + err);
                                }
                                res.send(
                                    (err === null) ? {msg: ''} : {msg: err}
                                );
                            });
                            db1.close();
                        });
                    });
                }
            });
        }
    }
});

/*
 * DELETE to deletecontent.
 */
router.delete('/deletecontent/:id', function (req, res) {
    var db = req.db;
    var collection = db.get('cmp_categories');
    var userToDelete = req.params.id;
    collection.remove({'_id': parseInt(userToDelete)}, function (err) {
        res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
    });
});

module.exports = router;