var express = require('express');
var router = express.Router();
var request = require('request');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.render('daywise', {title: 'Day Stats - Email Validation Stats'});
});
/*
 * GET stats.
 */
router.get('/getRabbitStats/:user', function (req, resp) {
    var docs = {};
    var total_req = 0;
    var maximum_req = 0;
    var ques = [];
    ques[0] = 'inreqQ';
    ques[1] = 'yahoodns_ispQ';
    ques[2] = 'aol_ispQ';
    ques[3] = 'hotmail_ispQ';
    ques[4] = 'others_ispQ';
    for (var i = 0; i < 5; i++) {
        total_req++;
        maximum_req++;
        requ(i);

        function requ(i) {
            request('http://test1:test1@' + '199.193.113.72' + ':15672/api/queues/%2f/' + ques[i] + '_' + req.params.user, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    docs[ques[i]] = JSON.parse(body).messages_persistent;
                } else {
                    docs[ques[i]] = 0;
                }
                total_req--;
                if (total_req === 0 && maximum_req === 5) {
                    resp.json(docs);
                }
            });
        }
    }
});
/*
 * GET stats.
 */
router.get('/getStats/:user/:date', function (req, res) {
    var db = req.db;
    var collection = db.get('emv_counts');
    if (req.params.user === 'all') {
        collection.find({'date': req.params.date}, {}, function (e, docs) {
            var i = 0, j = docs.length;
            var doc = {
                isp_wise: {
                    google: {income: 0, processed: 0, valid: 0, invalid: 0},
                    yahoodns: {income: 0, processed: 0, valid: 0, invalid: 0},
                    aol: {income: 0, processed: 0, valid: 0, invalid: 0},
                    hotmail: {income: 0, processed: 0, valid: 0, invalid: 0},
                    others: {income: 0, processed: 0, valid: 0, invalid: 0}
                },
                total: {income: 0, processed: 0, valid: 0, invalid: 0}
            };
            var total, isp_wise;
            while (i < j) {
                total = docs[i].total;
                var atleastOneRecordFlag = false;
                if (total !== undefined) {
                    if (total.income !== undefined) {
                        atleastOneRecordFlag = true;
                        doc.total.income += total.income;
                    }
                    if (total.processed !== undefined) {
                        atleastOneRecordFlag = true;
                        doc.total.processed += total.processed;
                    }
                    if (total.valid !== undefined) {
                        atleastOneRecordFlag = true;
                        doc.total.valid += total.valid;
                    }
                    if (total.invalid !== undefined) {
                        atleastOneRecordFlag = true;
                        doc.total.invalid += total.invalid;
                    }
                }
                isp_wise = docs[i].isp_wise;
                if (isp_wise !== undefined) {
                    total = isp_wise.google;
                    if (total !== undefined) {
                        if (total.income !== undefined) doc.isp_wise.google.income += total.income;
                        if (total.processed !== undefined) doc.isp_wise.google.processed += total.processed;
                        if (total.valid !== undefined) doc.isp_wise.google.valid += total.valid;
                        if (total.invalid !== undefined) doc.isp_wise.google.invalid += total.invalid;
                    }
                    total = isp_wise.yahoodns;
                    if (total !== undefined) {
                        if (total.income !== undefined) doc.isp_wise.yahoodns.income += total.income;
                        if (total.processed !== undefined) doc.isp_wise.yahoodns.processed += total.processed;
                        if (total.valid !== undefined) doc.isp_wise.yahoodns.valid += total.valid;
                        if (total.invalid !== undefined) doc.isp_wise.yahoodns.invalid += total.invalid;
                    }
                    total = isp_wise.aol;
                    if (total !== undefined) {
                        if (total.income !== undefined) doc.isp_wise.aol.income += total.income;
                        if (total.processed !== undefined) doc.isp_wise.aol.processed += total.processed;
                        if (total.valid !== undefined) doc.isp_wise.aol.valid += total.valid;
                        if (total.invalid !== undefined) doc.isp_wise.aol.invalid += total.invalid;
                    }
                    total = isp_wise.hotmail;
                    if (total !== undefined) {
                        if (total.income !== undefined) doc.isp_wise.hotmail.income += total.income;
                        if (total.processed !== undefined) doc.isp_wise.hotmail.processed += total.processed;
                        if (total.valid !== undefined) doc.isp_wise.hotmail.valid += total.valid;
                        if (total.invalid !== undefined) doc.isp_wise.hotmail.invalid += total.invalid;
                    }
                    total = isp_wise.others;
                    if (total !== undefined) {
                        if (total.income !== undefined) doc.isp_wise.others.income += total.income;
                        if (total.processed !== undefined) doc.isp_wise.others.processed += total.processed;
                        if (total.valid !== undefined) doc.isp_wise.others.valid += total.valid;
                        if (total.invalid !== undefined) doc.isp_wise.others.invalid += total.invalid;
                    }
                }
                i++;
            }
            if (atleastOneRecordFlag) {
                res.json([doc]);
            }
            else {
                res.json(docs);
            }
        });
    } else {
        collection.find({'user': req.params.user, 'date': req.params.date}, {}, function (e, docs) {
            res.json(docs);
        });
    }
});

module.exports = router;