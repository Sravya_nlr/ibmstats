var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    console.log("***********************"+req.ip);
    res.render('home', {title: 'Email Validation Stats'});
});

module.exports = router;
