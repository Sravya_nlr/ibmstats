/**
 * Created by sys050 on 31/3/17.
 */
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('LI_stats', {title: 'LI Details - Email Validation Stats'});
});
/*
 * GET domainlist.
 */
router.get('/LI_data', function (req, res) {

    var LIurl = "mongodb://tpuser:8Fhw6d.9Jl@103.18.248.11:59765/Linkedin_crawl?authSource=admin";
    MongoClient.connect(LIurl, function (err, db1) {
        if (err) {
            console.log("Errorr::", err);
        }
        else {
            console.log("Successfully authenticated to database");
            /*db1.collections(function(err, collections){
             console.log("collection names",collections);
             });*/
            var db = db1;
            var monthNames = [
                "Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul",
                "Aug", "Sept", "Oct",
                "Nov", "Dec"
            ];
            var day = "";
            // var db = req.lidb;
            var collection = db.collection('timewise_stats', function (err, response) {
                if (err) {
                    console.log("Error:::", err);
                }
                else {
                    console.log("responseeeeeeeeeeeeeeeeeeeeeeee");

                }
            });
            try {

                collection.find().sort({_id: -1}).limit(10).toArray(function (err, items) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {

                        console.log("linkedin response");
                        items.forEach(function (dt) {
                            var d1 = (dt._id) * 1000;
                            var date3 = new Date(d1);
                            day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                            dt._id = day;
                            if(dt.hits === undefined){
                                dt.hits=0;
                            }
                            if(dt.distance === undefined){
                                dt.distance=0;
                            }
                            if(dt.linkedin_true === undefined){
                                dt.linkedin_true=0;
                            }
                            if(dt.no_member === undefined){
                                dt.no_member=0;
                            }
                            if(dt.succ_attempts === undefined){
                                dt.succ_attempts =0;
                            }
                            if(dt.throttles === undefined){
                                dt.throttles=0;
                            }
                        });
                        db1.close();
                        res.send(items);

                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }
        }
    });

});
/*
 * POST to adddomain.
 */
/*router.post('/addDomain', function (req, res) {
 var db = req.db;
 var uuid = req.uuid;
 var collection = db.get('domain_master');
 var body = req.body;
 body["api_key"] = uuid.v1();
 body["priority"] = parseInt((body["priority"]));
 var insert = {};
 var Db = require('mongodb').Db,
 Server = require('mongodb').Server;
 var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

 db1.open(function(err, db1) {
 db1.eval('getNextSequenceValue("cmp_domain_master")', function (err, result) {
 if (err) {
 console.error(err);
 }
 insert["_id"] = result;

 insert["domain_name"] = body["domain_name"];
 insert["domainip"] = body["domain_ip"];
 insert["rbmq_username"] = "test1";
 insert["rbmq_password"] = "test1";
 insert["perday_capacity"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 var one_day_in_ms = 86400000;
 insert["sleep_eachmail"] = {
 google: insert["perday_capacity"].google!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].google) : 0,
 yahoodns: insert["perday_capacity"].yahoodns!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].yahoodns) : 0,
 hotmail: insert["perday_capacity"].hotmail!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].hotmail) : 0,
 aol: insert["perday_capacity"].aol!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].aol) : 0,
 others: insert["perday_capacity"].others!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].others) : 0
 };
 insert["remain_today"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 insert["status"] = {
 google: "inactive",
 yahoodns: "inactive",
 hotmail: "inactive",
 aol: "inactive",
 others: "inactive"
 };
 insert["start_hour"] = {
 google: 0,
 yahoodns: 0,
 hotmail: 0,
 aol: 0,
 others: 0
 };
 insert['mtastats_api_port'] = parseInt(body["api_port"]);

 collection.insert(insert, function (err, result) {
 if (err !== null) {
 console.log("err : " + err);
 }
 res.send(
 (err === null) ? {msg: ''} : {msg: err}
 );
 });
 });
 });
 });*/

/*
 * DELETE to deletedomain.
 */
/*router.delete('/deleteDomain/:id', function (req, res) {
 var db = req.db;
 var collection = db.get('domain_master');
 var userToDelete = req.params.id;
 collection.remove({'_id': parseInt(userToDelete)}, function (err) {
 res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
 });
 });*/

module.exports = router;
