/**
 * Created by sys050 on 30/3/17.
 */
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('ibm', {title: 'Gmail Validation Details - Email Validation Stats'});
});
/*
 * GET domainlist.
 */
var db = "";
function getTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var hour = date.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var minute = date.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }
    var dt = {
        minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
        dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
    };
    return dt;
}
var conf=require("./conf");
//var tmurl = "mongodb://tpuser:8Fhw6d.9Jl@103.18.248.11:59765/GooglePlusTest?authSource=admin";
// var ibmurl = "mongodb://tpuser:4Pkp4f.9Kv@209.58.169.65:27017/way2insights?authSource=admin";
var ibmurl = conf.ibmurl;
MongoClient.connect(ibmurl, function (err, db1) {
    if (err) {
        console.log("Errorr::", err);
    }
    else {
       // console.log("Successfully authenticated to truem database");
        db = db1;
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];
        var day = "";
        var date = getTime().dateStamp;
//	console.log(date);
        var collection = db.collection('active_users');
        router.get('/tot_data',function(req,res){
            db.collection("seed_master").count({
                esp: "gmail"
            }, function (err, respo) {
                if (err) {
                    throw err;
                }
                else {
                    db.collection("seed_master").count({
                        esp: "yahoo"
                    }, function (err, respo1) {
                        if (err) {
                            throw err;
                        }
                        else {
                            //console.log("*****",{GST:respo,YST:respo1});
                            res.send({GST:respo,YST:respo1})
                        }
                    });
                }
            });
        });
        router.get('/ibm_data', function (req, res) {
            var actArr = [];
            var i = 0;
            try {
                function act(i) {
                    //console.log(date,"dateeeeeeeeeeeeeeeeeeeeeeeeeeee");
                    collection.count({ESP: "gmail", Date: date - (i * 86400000)}, function (err, count) {
                        if (err) {
                            console.log("errorrrrrr", err);
                        }
                        else {
                           // console.log("responseeeee from ibm", count);
                            //res.send({"Gcount": count});
                            collection.count({ESP: "yahoo", Date: date - (i * 86400000)}, function (err, count1) {
                                if (err) {
                                    console.log("errorrrrrr", err);
                                }
                                else {
                                    db.collection("seed_master").count({
                                        esp: "gmail",
                                        "created.dateStamp": date - (i * 86400000)
                                    }, function (err, resp) {
                                        if (err) {
                                            throw err;
                                        }
                                        else {
                                            db.collection("seed_master").count({
                                                esp: "yahoo",
                                                "created.dateStamp": date - (i * 86400000)
                                            }, function (err, resp1) {
                                                if (err) {
                                                    throw err;
                                                }
                                                else {
                                                    var date3 = new Date(date - (i * 86400000));
                                                    // console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy",dt._id+'000'.getMonth());
                                                    day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                                                    //console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy", day)

                                                    actArr.push({
                                                        Date: day,
                                                        "Gcount": count,
                                                        "Ycount": count1,
                                                        "GS": resp,
                                                        "YS": resp1,
                                                        "GST": (count - resp),
                                                        "YST": (count1 - resp1)
                                                    });
                                                    i++;
                                                    if (i < 5) {
                                                        act(i)
                                                    }
                                                    else {
                                                        //console.log(actArr);
                                                        res.send(actArr)
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
            catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }
            act(i);

        });
        router.get('/truem_data2', function (req, res) {

            try {
                collection.find({"response": null}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from data_truem2", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
        router.get('/truem_data3', function (req, res) {

            try {
                collection.find({"response": "invalid"}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from data_truem33333", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
        router.get('/truem_data4', function (req, res) {
            try {
                collection.find({"response": {"$nin": [null, "invalid"]}}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from data_truem444444", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
    }
});
/*
 * POST to adddomain.
 */
/*router.post('/addDomain', function (req, res) {
 var db = req.db;
 var uuid = req.uuid;
 var collection = db.get('domain_master');
 var body = req.body;
 body["api_key"] = uuid.v1();
 body["priority"] = parseInt((body["priority"]));
 var insert = {};
 var Db = require('mongodb').Db,
 Server = require('mongodb').Server;
 var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

 db1.open(function(err, db1) {
 db1.eval('getNextSequenceValue("cmp_domain_master")', function (err, result) {
 if (err) {
 console.error(err);
 }
 insert["_id"] = result;

 insert["domain_name"] = body["domain_name"];
 insert["domainip"] = body["domain_ip"];
 insert["rbmq_username"] = "test1";
 insert["rbmq_password"] = "test1";
 insert["perday_capacity"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 var one_day_in_ms = 86400000;
 insert["sleep_eachmail"] = {
 google: insert["perday_capacity"].google!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].google) : 0,
 yahoodns: insert["perday_capacity"].yahoodns!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].yahoodns) : 0,
 hotmail: insert["perday_capacity"].hotmail!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].hotmail) : 0,
 aol: insert["perday_capacity"].aol!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].aol) : 0,
 others: insert["perday_capacity"].others!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].others) : 0
 };
 insert["remain_today"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 insert["status"] = {
 google: "inactive",
 yahoodns: "inactive",
 hotmail: "inactive",
 aol: "inactive",
 others: "inactive"
 };
 insert["start_hour"] = {
 google: 0,
 yahoodns: 0,
 hotmail: 0,
 aol: 0,
 others: 0
 };
 insert['mtastats_api_port'] = parseInt(body["api_port"]);

 collection.insert(insert, function (err, result) {
 if (err !== null) {
 console.log("err : " + err);
 }
 res.send(
 (err === null) ? {msg: ''} : {msg: err}
 );
 });
 });
 });
 });*/

/*
 * DELETE to deletedomain.
 */
/*router.delete('/deleteDomain/:id', function (req, res) {
 var db = req.db;
 var collection = db.get('domain_master');
 var userToDelete = req.params.id;
 collection.remove({'_id': parseInt(userToDelete)}, function (err) {
 res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
 });
 });*/

module.exports = router;
