/**
 * Created by sys050 on 30/3/17.
 */
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var conf=require("./conf");
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('daywise_count', {title: 'FB Validation Details - Email Validation Stats'});
});
/*
 * GET domainlist.
 */
function getTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var hour = date.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var minute = date.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }
    var dt = {
        minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
        dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
    };
    return dt;
}
var db = "";
var ibmurl = conf.ibmurl;
// var tmurl = "mongodb://tpuser:8Fhw6d.9Jl@103.18.248.11:59765/GooglePlusTest?authSource=admin";
MongoClient.connect(ibmurl, function (err, db1) {
    if (err) {
        console.log("Errorr::", err);
    }
    else {
        //console.log("Successfully authenticated to truem database");
        db = db1;
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sept", "Oct",
            "Nov", "Dec"
        ];
        var day = "";
        var date = getTime().dateStamp;
        var collection = db.collection('ext1_data');
        router.get('/daywise_count_data', function (req, res) {
            try {
                var i=0;
                var j=0;
                var datArr=[];
                var datArr1=[];
                inc(i);
                //inc1(j);
                function inc(i){
                    act(i,0,0,function(err,resp){
                        datArr.push(resp);
                        i++;
                        if(i < 5){
                            inc(i);
                        }
                        else{
                            //console.log("datArr",datArr);
                            res.send(datArr);
                        }
                    });
                }
               /* function inc1(j){
                    act1(j,0,1,function(err,res){
                        datArr1.push(res);
                        j++;
                        if(j < 5){
                            inc1(j);
                        }
                        else{
                            console.log("datArr1",datArr1);
                        }
                    });
                }*/
                function act(i,app_id,ESP,cb){
                    collection.find({ESP:ESP,created_dt:date - (i * 86400000)}).count(function (err, count) {
                        if (err) {
                            console.log("errorrrrrr", err);
                        }
                        else {
                            var date3 = new Date(date - (i * 86400000));
                            // console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy",dt._id+'000'.getMonth());
                            day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                            //console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy", day)
                            //console.log("responseeeee from fb_valid_data", {Date:day,"Gcount": count});
                            cb(null,{Date:day,"Gcount": count});
                        }
                    });
                }
               /* function act1(i,app_id,ESP,cb){
                    collection.find({app_id:app_id,ESP:ESP,created_dt:date - (i * 86400000)}).count(function (err, count) {
                        if (err) {
                            console.log("errorrrrrr", err);
                        }
                        else {
                            var date3 = new Date(date - (i * 86400000));
                            // console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy",dt._id+'000'.getMonth());
                            day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                            //console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy", day)
                            // console.log("responseeeee from fb_valid_data", {Date:day,"Gcount": count});
                            cb(null,{Date:day,"Ycount": count});
                        }
                    });
                }*/

            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }
        });
        router.get('/daywise_count_Ydata', function (req, res) {
            try {
                var i=0;
                var j=0;
                var datArr=[];
                var datArr1=[];
                inc(i);
                function inc(i){
                    act(i,0,1,function(err,resp){
                        datArr.push(resp);
                        i++;
                        if(i < 5){
                            inc(i);
                        }
                        else{
                            //console.log("datArr",datArr);
                            res.send(datArr);
                        }
                    });
                }
                function act(i,app_id,ESP,cb){
                    collection.find({ESP:ESP,created_dt:date - (i * 86400000)}).count(function (err, count) {
                        if (err) {
                            console.log("errorrrrrr", err);
                        }
                        else {
                            var date3 = new Date(date - (i * 86400000));
                            // console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy",dt._id+'000'.getMonth());
                            day = monthNames[date3.getMonth()] + '  ' + date3.getDate() + ' , ' + date3.getFullYear();
                            //console.log("dayyyyyyyyyyyyyyyyyyyyyyyyyyyy", day)
                            // console.log("responseeeee from fb_valid_data", {Date:day,"Gcount": count});
                            cb(null,{Date:day,"Ycount": count});
                        }
                    });
                }

            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }
        });
       /* router.get('/fb_valid_data2', function (req, res) {

            try {
                collection.find({c:1}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from fb_valid_data2", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
        router.get('/fb_valid_data3', function (req, res) {

            try {
                collection.find({c:1,result:'v'}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from fb_valid_data3", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
        router.get('/fb_valid_data4', function (req, res) {
            try {
                collection.find({c:1,result:'i'}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from fb_valid_data4", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });
        router.get('/fb_valid_data5', function (req, res) {
            try {
                collection.find({c:1,result:'h'}).count(function (err, count) {
                    if (err) {
                        console.log("errorrrrrr", err);
                    }
                    else {
                        console.log("responseeeee from fb_valid_data5", count);
                        res.send({"count": count});
                    }
                });
            } catch (errrrrrrrrrrrrrrrrrrrrrrrrrrr) {
                console.error(errrrrrrrrrrrrrrrrrrrrrrrrrrr.stack);
                process.exit(0);
            }

        });*/
    }
});
/*
 * POST to adddomain.
 */
/*router.post('/addDomain', function (req, res) {
 var db = req.db;
 var uuid = req.uuid;
 var collection = db.get('domain_master');
 var body = req.body;
 body["api_key"] = uuid.v1();
 body["priority"] = parseInt((body["priority"]));
 var insert = {};
 var Db = require('mongodb').Db,
 Server = require('mongodb').Server;
 var db1 = new Db('emailcrawl_internal', new Server('199.193.113.72', 27017));

 db1.open(function(err, db1) {
 db1.eval('getNextSequenceValue("cmp_domain_master")', function (err, result) {
 if (err) {
 console.error(err);
 }
 insert["_id"] = result;

 insert["domain_name"] = body["domain_name"];
 insert["domainip"] = body["domain_ip"];
 insert["rbmq_username"] = "test1";
 insert["rbmq_password"] = "test1";
 insert["perday_capacity"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 var one_day_in_ms = 86400000;
 insert["sleep_eachmail"] = {
 google: insert["perday_capacity"].google!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].google) : 0,
 yahoodns: insert["perday_capacity"].yahoodns!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].yahoodns) : 0,
 hotmail: insert["perday_capacity"].hotmail!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].hotmail) : 0,
 aol: insert["perday_capacity"].aol!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].aol) : 0,
 others: insert["perday_capacity"].others!==0 ? parseFloat(one_day_in_ms / insert["perday_capacity"].others) : 0
 };
 insert["remain_today"] = {
 google: parseInt(body["gmail_cap"]),
 yahoodns: parseInt(body["yahoo_cap"]),
 hotmail: parseInt(body["hotmail_cap"]),
 aol: parseInt(body["aol_cap"]),
 others: parseInt(body["others_cap"])
 };
 insert["status"] = {
 google: "inactive",
 yahoodns: "inactive",
 hotmail: "inactive",
 aol: "inactive",
 others: "inactive"
 };
 insert["start_hour"] = {
 google: 0,
 yahoodns: 0,
 hotmail: 0,
 aol: 0,
 others: 0
 };
 insert['mtastats_api_port'] = parseInt(body["api_port"]);

 collection.insert(insert, function (err, result) {
 if (err !== null) {
 console.log("err : " + err);
 }
 res.send(
 (err === null) ? {msg: ''} : {msg: err}
 );
 });
 });
 });
 });*/

/*
 * DELETE to deletedomain.
 */
/*router.delete('/deleteDomain/:id', function (req, res) {
 var db = req.db;
 var collection = db.get('domain_master');
 var userToDelete = req.params.id;
 collection.remove({'_id': parseInt(userToDelete)}, function (err) {
 res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
 });
 });*/

module.exports = router;
