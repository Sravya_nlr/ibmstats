var userListData = [];
//var oFCKeditor;

$(document).ready(function () {
    //CKEDITOR.replace( 'editorContent' );
    $('#content_details_wmp').addClass('active');
    populateTable();
    // Add User button click
    //$('#btnAddContent').on('click', addContent);
    // Delete User link click
    $('#userList table tbody').on('click', 'td a.linkdeletecontent', deleteConent);

});

$(document).on('click', '#edit_fromname', function(e) {
    e.preventDefault();
    $("#fromname").html("<input id='fromname_input' type='text' value='"+$("#fromname_").text()+"' />&nbsp;&nbsp;<a href='#' id='save_fromname'>save</a>");
});

$(document).on('click', '#save_fromname', function(e) {
    e.preventDefault();
    $("#fromname").html("<span id='fromname_'>"+$("#fromname_input").val()+"</span>&nbsp;&nbsp;<a href='#' id='edit_fromname'>edit</a>");
    $.post('/content_details_wmp/saveValue/'+ $("#domain").text() +"/fromname/"+$("#fromname_").text(), function (data) {

    });
});

$(document).on('click', '#edit_subject', function(e) {
    e.preventDefault();
    $("#subject").html("<input class='input' style='width: 600px;' id='subject_input' type='text' value='"+$("#subject_").text()+"' />&nbsp;&nbsp;<a href='#' id='save_subject'>save</a>");

});

$(document).on('click', '#save_subject', function(e) {
    e.preventDefault();
    $("#subject").html("<span id='subject_'>"+$("#subject_input").val()+"</span>&nbsp;&nbsp;<a href='#' id='edit_subject'>edit</a>");
    $.post('/content_details_wmp/saveValue/'+ $("#domain").text() +"/subject/"+$("#subject_").text(), function (data) {

    });
});

$(document).on('click', '#save_content', function(e) {
    e.preventDefault();
    console.log("save", CKEDITOR.instances.editorContent.getData().replace(/>\s+</g, '><'));
    //console.log("save", CKEDITOR.instances.editorContent.getData());
    $.post('/content_details_wmp/saveValue/'+ $("#domain").text() +"/body/"+"value", {val : CKEDITOR.instances.editorContent.getData().replace(/>\s+</g, '><')}, function (data) {

    });
});

function populateTable() {
    var tableContent = '';
    $.getJSON('/content_details_wmp/contentlist', function (data) {
        userListData = data;
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td><a href="#" class="linkshowcontent" rel="' + this.domain_name + '">' + this.domain_name + '</a></td>';
            tableContent += '<td><a href="#" class="linkdeletecontent" rel="' + this.domain_name + '">delete</a></td>';
            tableContent += '</tr>';
        });

        $('#userList table tbody').html(tableContent);
        $('#userList table tbody').on('click', 'td a.linkshowcontent', showContentInfo);
    });
}

// Show Content Info
function showContentInfo(event) {
    event.preventDefault();
    $('#category').text('');
    $('#fromname').text('');
    $('#subject').text('');
    $('#content').text('');
    var thisContentName = $(this).attr('rel');
    var arrayPosition = userListData.map(function (arrayItem) {
        return arrayItem.domain_name;
    }).indexOf(thisContentName);
    var thisUserObject = userListData[arrayPosition];

    $('#domain').text(thisUserObject.domain_name);
    $("#fromname").html("<span id='fromname_'></span>");
    $('#fromname_').text((thisUserObject.wmp_content && thisUserObject.wmp_content.fromname)?thisUserObject.wmp_content.fromname:'');
    $('#fromname').append("&nbsp;&nbsp;<a id='edit_fromname' href='#'>edit</a>");
    $("#subject").html("<span id='subject_'></span>");
    $('#subject_').text((thisUserObject.wmp_content && thisUserObject.wmp_content.subject)?thisUserObject.wmp_content.subject:'');
    $('#subject').append("&nbsp;&nbsp;<a id='edit_subject' href='#'>edit</a>");
    $('#editorContent').text((thisUserObject.wmp_content && thisUserObject.wmp_content.content)?thisUserObject.wmp_content.content:'');
    $('#content').append("<a href='#' id='save_content'>save</a>");
    //console.log("BEFOREeditorContent", $('#editorContent').text());

    if (CKEDITOR.instances["editorContent"])
        CKEDITOR.instances["editorContent"].destroy(true);

    CKEDITOR.config.height = 600;     // 850 pixels wide.
    CKEDITOR.config.width = '100%';   // CSS unit.
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.fillEmptyBlocks = false;
    CKEDITOR.config.tabSpaces = 0;
    CKEDITOR.config.basicEntities = false;
    CKEDITOR.config.forcePasteAsPlainText = true;
    CKEDITOR.config.ToolbarCanCollapse = true;

    CKEDITOR.config.autoParagraph = false;

    CKEDITOR.config.fillEmptyBlocks = false;
    CKEDITOR.config.shiftEnterMode = false;
    CKEDITOR.config.enterMode = false;
    //CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_P;
    //CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;

    //CKEDITOR.config.extraPlugins = 'widget';
    //CKEDITOR.config.extraPlugins = 'dialog';
    //CKEDITOR.config.extraPlugins = 'placeholder';

    //CKEDITOR.dtd.$removeEmpty.p= 0;

    CKEDITOR.config.entities = false;
    CKEDITOR.config.basicEntities = false;
    //CKEDITOR.config.entities_latin = false;
    //CKEDITOR.config.entities_greek = false;

    //CKEDITOR.config.language = 'en';

    CKEDITOR.config.htmlEncodeOutput = false;

    CKEDITOR.replace( 'editorContent' );

    //$('#editorContent').html((thisUserObject.wmp_content && thisUserObject.wmp_content.content)?''+thisUserObject.wmp_content.content+'':'');

    //oFCKeditor = new FCKeditor('editor');
    //oFCKeditor.BasePath = "javascripts/fckeditor/";
    //oFCKeditor.ProcessHTMLEntities = false;
    //oFCKeditor.Width = 800;
    //oFCKeditor.Height = 500;
    //oFCKeditor.Value = document.getElementById("editorContent").value;
    //oFCKeditor.Create();

    console.log("AFTEReditorContent", CKEDITOR.instances.editorContent.getData());
}

// Add Content
/*function addContent(event) {
    event.preventDefault();

    if ($('#inputDomain').val() === '' || $('#inputFromName').val() === '' || $('#inputSubject').val() === '' || $('#inputContent').val() === '') {
        alert('Please fill in all fields');
        return false;
    } else {
        var newContent = {
            'domain': $('#inputDomain').val().trim(),
            'fromname': $('#inputFromName').val().trim(),
            'subject': $('#inputSubject').val().trim(),
            'content': $('#inputContent').val().trim().replace(/>\s+</g, "><")
        };
        $.post("/content_details_wmp/addcontent/", newContent, function (response) {
            if (response.msg === '') {
                $('#inputDomain').val('');
                $('#inputFromName').val('');
                $('#inputSubject').val('');
                $('#inputContent').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
}*/

// Delete Content
function deleteConent(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this category?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/content_details_wmp/deletecontent/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            populateTable();
        });
    }
    else {
        return false;
    }
}