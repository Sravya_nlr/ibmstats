$(document).ready(function () {
    $('#user_settings').addClass('active');
    populateSettingsTable();
    $('#userList table tbody').on('click', 'td a.linkedituser', linkEditUser);
    $('#userList table tbody').on('click', 'td a.linksaveuser', linkSaveUser);
});

function populateSettingsTable() {
    var tableContent = '';
    $.getJSON('/user_settings/userlist', function (data) {
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td>' + this.name + '</td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td>' + this.age + '</td>';
            tableContent += '<td>' + this.location + '</td>';
            tableContent += '<td>' + this.gender + '</td>';
            tableContent += '<td>' + this.priority + '</td>';
            tableContent += '<td><a href="#" class="linkedituser" rel="' + this._id + '">edit</a></td>';
            //tableContent += '<td>' + this.api_key + '</td>';
        });

        $('#userList table tbody').html(tableContent);
    });
}


function linkEditUser(event) {
    event.preventDefault();
    var i = 0;
    $(this).parent().parent().children('td').each(function () {
        //alert($(this).html());
        switch (i) {
            case 0 :
                $(this).html("<input style='width: 180px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 1 :
                $(this).html("<input style='width: 200px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 2 :
                $(this).html("<input style='width: 22px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 3 :
                $(this).html("<input style='width: 90px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 4 :
                if ($(this).text() === 'Male')
                    $(this).html("<select><option value='male' selected>Male</option><option value='female'>Female</option></select>");
                else
                    $(this).html("<select><option value='female' selected>Female</option><option value='male'>Male</option></select>");
                break;
            case 5 :
                $(this).html("<input style='width: 35px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 6 :
                $(this).html("<a href='#' style='color: green;' class='linksaveuser' rel=" + $(this).children('a').attr('rel') + ">save</a>");
                break;
            //case 7 :
            //    break;
        }
        i++;
    });
}

function linkSaveUser(event) {
    event.preventDefault();
    var i = 0;
    var user = {};
    var temp;
    $(this).parent().parent().children('td').each(function () {
        switch (i) {
            case 0 :
                temp = $(this).children('input').val().trim();
                $(this).html(capitalize(temp, true));
                user['name'] = capitalize(temp, true);
                break;
            case 1 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                user['email'] = temp;
                break;
            case 2 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                user['age'] = temp;
                break;
            case 3 :
                temp = $(this).children('input').val().trim();
                $(this).html(capitalize(temp, true));
                user['location'] = capitalize(temp, true);
                break;
            case 4 :
                temp = $(this).children('select').val().trim();
                if (temp === 'male')
                    $(this).html("Male");
                else
                    $(this).html("Female");
                user['gender'] = capitalize(temp);
                break;
            case 5 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                user['priority'] = temp;
                break;
            case 6 :
                $(this).html("<a href='#' class='linkedituser' rel='" + $(this).children('a').attr('rel') + "'>edit</a>");
                break;
            //case 7 :
            //    break;
        }
        i++;
    });
    user['username'] = user['email'];

    $.post("/user_settings/saveuser/" + $(this).attr('rel'), user, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
}

function capitalize(str, all) {
    var lastResponded;
    return str.toLowerCase().replace(all ? /[^']/g : /^\S/, function (lower) {
        var upper = lower.toUpperCase(),
            result;
        result = lastResponded ? lower : upper;
        lastResponded = upper !== lower;
        return result;
    });
}