/**
 * Created by sys050 on 31/3/17.
 */

var LIListData2 = [];

$(document).ready(function () {
    $('#LI_stats2').addClass('active');
    populateTable();
    // Add User button click
    // $('#btnaddDomain').on('click', addDomain);
    // // Delete User link click
    // $('#userList table tbody').on('click', 'td a.linkdeleteDomain', deleteDomain);
});

function populateTable() {
    // alert("populate LI2")

    var tableContent1 = '';
    $.getJSON('/LI_stats2/LI_data2', function (data) {
        LIListData2 = data;


        $.each(data, function () {
            tableContent1 += '<tr>';
            tableContent1 += '<td>' + this._id + '</td>';
            tableContent1 += '<td>' + this.hold + '</td>';
            tableContent1 += '<td>' + this.internal_error + '</td>';
            tableContent1 += '<td>' + this.invalid_request + '</td>';
            tableContent1 += '<td>' + this.other_result + '</td>';
            tableContent1 += '<td>' + this.tokens_generated + '</td>';
            tableContent1 += '<td>' + this.token_expired + '</td>';
            tableContent1 += '<td>' + this.no_access + '</td>';

            tableContent1 += '</tr>';
        });
        $('#userList table tbody').html(tableContent1);
    });
}
/*

 // Add Domain
 function addDomain(event) {
 event.preventDefault();

 var errorCount = 0;
 $('#addDomain input').each(function (index, val) {
 if ($(this).val() === '') {
 errorCount++;
 }
 });

 if (errorCount === 0) {
 var newDomain = {
 'domain_name': $('#addDomain fieldset input#inputDomainDomainName').val().trim(),
 'domain_ip': $('#addDomain fieldset input#inputDomainDomainIP').val().trim(),
 'gmail_cap': $('#addDomain fieldset input#inputDomainGoogleCapacity').val().trim(),
 'yahoo_cap': $('#addDomain fieldset input#inputDomainYahooCapacity').val().trim(),
 'hotmail_cap': $('#addDomain fieldset input#inputDomainHotmailCapacity').val().trim(),
 'aol_cap': $('#addDomain fieldset input#inputDomainAOLCapacity').val().trim(),
 'others_cap': $('#addDomain fieldset input#inputDomainOthersCapacity').val().trim(),
 'api_port': $('#addDomain fieldset input#inputDomainAPIPort').val().trim()
 };
 $.post("/domains/addDomain", newDomain, function (response) {
 if (response.msg === '') {
 $('#addDomain fieldset input').val('');
 populateTable();
 }
 else {
 alert('Error: ' + response.msg);
 }
 });
 }
 else {
 alert('Please fill in all fields');
 return false;
 }
 }

 // Delete Domain
 function deleteDomain(event) {

 event.preventDefault();
 var confirmation = confirm('Are you sure you want to delete this user?');
 if (confirmation === true) {
 $.ajax({
 type: 'DELETE',
 url: '/domains/deleteDomain/' + $(this).attr('rel')
 }).done(function (response) {
 if (response.msg === '') {
 }
 else {
 alert('Error: ' + response.msg);
 }
 populateTable();
 });
 }
 else {
 return false;
 }
 }*/
