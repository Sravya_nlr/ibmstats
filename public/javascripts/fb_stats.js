/**
 * Created by sys050 on 30/3/17.
 */

var FBData = [];
$('#refresh').on('click', function () {
    populateTable();
});
$(document).ready(function () {
    $('#fb_stats').addClass('active');
    populateTable();
    // Add User button click
    // $('#btnaddDomain').on('click', addDomain);
    // // Delete User link click
    // $('#userList table tbody').on('click', 'td a.linkdeleteDomain', deleteDomain);
});

function populateTable() {
    var image = new Image();
    // image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    image.src = 'https://media.giphy.com/media/IB9foBA4PVkKA/giphy.gif';
    $('#refresh').attr('src', image.src);
    var tableContent = '';
    $.getJSON('/top_domains/dom_data', function (data) {
        FBData = data;
        image.src = 'images/icon-refresh.png';
        console.log(data);
        $('#refresh').attr('src', image.src);
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td>' + this.domain + '</td>';
            tableContent += '<td>' + this.volume + '</td>';
            tableContent += '<td>' + this.inbox + '</td>';
            tableContent += '<td>' + this.spam + '</td>';
            tableContent += '<td>' + this.inbp + '</td>';
            tableContent += '<td>' + this.spp + '</td>';
            tableContent += '</tr>';
        });
        $('#userList table tbody').html(tableContent);
    });
}
/*

// Add Domain
function addDomain(event) {
    event.preventDefault();

    var errorCount = 0;
    $('#addDomain input').each(function (index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    if (errorCount === 0) {
        var newDomain = {
            'domain_name': $('#addDomain fieldset input#inputDomainDomainName').val().trim(),
            'domain_ip': $('#addDomain fieldset input#inputDomainDomainIP').val().trim(),
            'gmail_cap': $('#addDomain fieldset input#inputDomainGoogleCapacity').val().trim(),
            'yahoo_cap': $('#addDomain fieldset input#inputDomainYahooCapacity').val().trim(),
            'hotmail_cap': $('#addDomain fieldset input#inputDomainHotmailCapacity').val().trim(),
            'aol_cap': $('#addDomain fieldset input#inputDomainAOLCapacity').val().trim(),
            'others_cap': $('#addDomain fieldset input#inputDomainOthersCapacity').val().trim(),
            'api_port': $('#addDomain fieldset input#inputDomainAPIPort').val().trim()
        };
        $.post("/domains/addDomain", newDomain, function (response) {
            if (response.msg === '') {
                $('#addDomain fieldset input').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
}

// Delete Domain
function deleteDomain(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/domains/deleteDomain/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            populateTable();
        });
    }
    else {
        return false;
    }
}*/
