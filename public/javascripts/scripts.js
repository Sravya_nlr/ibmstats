var credentials = {};

$(document).on('click', '#saveOAuth', function() {
    credentials['user-agent'] = $('#user-agent').val();
    credentials['crumb'] = $('#crumb').val();
    credentials['cookie'] = $('#cookie').val();
    $.post("/settings/changeOauth", credentials, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
        $('[data-toggle="popover"]').popover('hide');
    });
});

$(document).ready(function () {
    $.getJSON('/settings/oauthStatus', function (data) {
        if(data.status==='yes') {
            $('#index_options').prepend('<li><a data-html="true"' +
                ' data-content="<div><input id=\'user-agent\' placeholder=\'Enter User-Agent\' type=\'text\' /><br /><br /><input id=\'crumb\' placeholder=\'Enter Crumb\' type=\'text\' /><br /><br /><input id=\'cookie\' placeholder=\'Enter Cookie\' type=\'text\' /><br /><br /><input id=\'saveOAuth\' style=\'padding-left: 56px;padding-right: 56px;\' type=\'button\' value=\'save\' /></div>" ' +
                ' title="Enter the new Cookies: " data-toggle="popover" data-placement="bottom" href="#" style="color: #FF0000">YQL Cookies expired!</a></li>');
            $('[data-toggle="popover"]').popover();
        } else {
            $('#index_options').prepend('<li><a data-html="true"' +
                ' data-content="<div><input id=\'user-agent\' placeholder=\'Enter User-Agent\' type=\'text\' /><br /><br /><input id=\'crumb\' placeholder=\'Enter Crumb\' type=\'text\' /><br /><br /><input id=\'cookie\' placeholder=\'Enter Cookie\' type=\'text\' /><br /><br /><input id=\'saveOAuth\' style=\'padding-left: 56px;padding-right: 56px;\' type=\'button\' value=\'save\' /></div>" ' +
                ' title="Enter the new Cookies: " data-toggle="popover" data-placement="bottom" href="#" style="color: #00FF00">YQL Cookies alive!</a></li>');
            $('[data-toggle="popover"]').popover();
        }
    });
});