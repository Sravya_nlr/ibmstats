$(document).ready(function () {
    $('#daywise_stats').addClass('active');
    $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", new Date());
    fillUsers();
    populateTable();
    populateTable2();
    //alert($('#datepicker').val());
    $('#datepicker').on('change', function () {
        populateTable();
    });
    $('#user_wise').on('change', function () {
        populateTable();
    });
    $('#user_wise_rabbit').on('change', function () {
        populateTable2();
    });
    $('#datepicker').bind("enterKey", function (e) {
        populateTable();
    });
    $('#datepicker').keyup(function (e) {
        if (e.keyCode == 13)
            $(this).trigger("enterKey");
    });

    $('#refresh').on('click', function () {
        populateTable();
    });

    $('#refresh2').on('click', function () {
        populateTable2();
    });
});

function fillUsers() {
    $.getJSON('/users/userlist', function (data) {
        $.each(data, function () {
            $('#user_wise').append($('<option>', {value: this.email, text: this.email}));
            $('#user_wise_rabbit').append($('<option>', {value: this.email, text: this.email}));
        });
    });
}

function populateTable() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    $('#refresh').attr('src', image.src);
    var tableContent = '';
    $.getJSON('/daywise/getStats/' + $('#user_wise').val() + '/' + $('#datepicker').val(), function (data) {
        var i = 0;
        var temp;
        var google;
        var yahoodns;
        var aol;
        var hotmail;
        var others;
        var total;
        if (data.length !== 0) {
            $.each(data, function () {
                temp = data[i].isp_wise;
                if (typeof temp !== 'undefined') {
                    google = temp.google;
                    yahoodns = temp.yahoodns;
                    aol = temp.aol;
                    hotmail = temp.hotmail;
                    others = temp.others;
                }
                total = data[i].total;

                tableContent += '<tr>';
                tableContent += '<td><strong>requests</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.income === 'undefined') ? 0 : google.income) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.income === 'undefined') ? 0 : yahoodns.income) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.income === 'undefined') ? 0 : aol.income) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.income === 'undefined') ? 0 : hotmail.income) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.income === 'undefined') ? 0 : others.income) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.income === 'undefined') ? 0 : total.income) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>responses</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.processed === 'undefined') ? 0 : google.processed) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.processed === 'undefined') ? 0 : yahoodns.processed) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.processed === 'undefined') ? 0 : aol.processed) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.processed === 'undefined') ? 0 : hotmail.processed) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.processed === 'undefined') ? 0 : others.processed) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.processed === 'undefined') ? 0 : total.processed) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>valid</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.valid === 'undefined') ? 0 : google.valid) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.valid === 'undefined') ? 0 : yahoodns.valid) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.valid === 'undefined') ? 0 : aol.valid) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.valid === 'undefined') ? 0 : hotmail.valid) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.valid === 'undefined') ? 0 : others.valid) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.valid === 'undefined') ? 0 : total.valid) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>invalid</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.invalid === 'undefined') ? 0 : google.invalid) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.invalid === 'undefined') ? 0 : yahoodns.invalid) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.invalid === 'undefined') ? 0 : aol.invalid) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.invalid === 'undefined') ? 0 : hotmail.invalid) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.invalid === 'undefined') ? 0 : others.invalid) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.invalid === 'undefined') ? 0 : total.invalid) + '</td>';
                tableContent += '</tr>';
                i++;
            });
        }
        else {
            tableContent += '<tr><td colspan="7">No Records found!</tr>';
        }
        $('#userList table tbody').html(tableContent);
        var image = new Image();
        image.src = 'images/icon-refresh.png';
        $('#refresh').attr('src', image.src);
    });
}


function populateTable2() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    $('#refresh2').attr('src', image.src);
    var tableContent = '';
    if($('#user_wise_rabbit').val()==='--- select user ---') {
        tableContent += '<tr><td colspan="7">Please select one of the users!</tr>';
        $('#userList2 table tbody').html(tableContent);
        var image = new Image();
        image.src = 'images/icon-refresh.png';
        $('#refresh2').attr('src', image.src);
    } else {
        $.getJSON('/daywise/getRabbitStats/' + $('#user_wise_rabbit').val(), function (data) {
            tableContent += '<tr>';
            tableContent += '<td><strong>inreqQ</strong></td>';
            tableContent += '<td>' + data.inreqQ + '</td>';
            tableContent += '</tr>';
            tableContent += '<tr>';
            tableContent += '<td><strong>yahoodns_ispQ</strong></td>';
            tableContent += '<td>' + data.yahoodns_ispQ + '</td>';
            tableContent += '</tr>';
            tableContent += '<tr>';
            tableContent += '<td><strong>hotmail_ispQ</strong></td>';
            tableContent += '<td>' + data.hotmail_ispQ + '</td>';
            tableContent += '</tr>';
            tableContent += '<tr>';
            tableContent += '<td><strong>aol_ispQ</strong></td>';
            tableContent += '<td>' + data.aol_ispQ + '</td>';
            tableContent += '</tr>';
            tableContent += '<tr>';
            tableContent += '<td><strong>others_ispQ</strong></td>';
            tableContent += '<td>' + data.others_ispQ + '</td>';
            tableContent += '</tr>';
            
            $('#userList2 table tbody').html(tableContent);
            var image = new Image();
            image.src = 'images/icon-refresh.png';
            $('#refresh2').attr('src', image.src);
        });
    }
}