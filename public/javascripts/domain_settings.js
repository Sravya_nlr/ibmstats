$(document).ready(function () {
    $('#domain_settings').addClass('active');
    fillDomains();
    populateSettingsTable();
    $('#domain_wise').on('change', populateSettingsTable);
    $('#selected_isp').on('change', populateSettingsTable);
    //$('#userList table tbody').on('click', 'td a.linkedituserStatus', linkEditDomainStatus);
    //$('#userList table tbody').on('click', 'td a.linkedituserCapacity', linkEditDomainCapacity);
    //$('#userList table tbody').on('click', 'td a.linkedituserStartHour', linkEditDomainStartHour);
    //$('#userList table tbody').on('click', 'td a.linksaveuserStatus', linkSaveDomainStatus);
    //$('#userList table tbody').on('click', 'td a.linksaveuserCapacity', linkSaveDomainCapacity);
    //$('#userList table tbody').on('click', 'td a.linksaveuserStartHour', linkSaveDomainStartHour);
});

$(document).on('click', '#edit_vi', viEdit);
$(document).on('click', '#edit_wmp', wmpEdit);
$(document).on('click', '#save_vi', viSave);
$(document).on('click', '#save_wmp', wmpSave);

function viEdit() {
    if ($('#domain_status').text() === 'active')
        $('#domain_status').html('<input type="radio" name="status" value="active" checked> Active <input type="radio" name="status" value="inactive"> Inactive');
    else
        $('#domain_status').html('<input type="radio" name="status" value="active"> Active <input type="radio" name="status" value="inactive" checked> Inactive');
    $('#domain_capacity').html("<input id='dom_capacity' style='width: 100px;' type='text' value='" + $('#domain_capacity').text() + "'/>");
    $('#domain_startHour').html("<input id='dom_start_hour' data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $('#domain_startHour').text() + "'/>");
    $('#domain_api_port').html("<input id='dom_api_port' style='width: 100px;' type='text' value='" + $('#domain_api_port').text() + "'/>");
    if ($('#domain_cont_type').text() === 'cat_wise')
        $('#domain_cont_type').html('<input type="radio" name="dom_cont_type" value="none"> None <input type="radio" name="dom_cont_type" value="dom_wise"> Domain wise <input type="radio" name="dom_cont_type" value="cat_wise" checked> Category wise');
    else if ($('#domain_cont_type').text() === 'dom_wise')
        $('#domain_cont_type').html('<input type="radio" name="dom_cont_type" value="none"> None <input type="radio" name="dom_cont_type" value="dom_wise" checked> Domain wise <input type="radio" name="dom_cont_type" value="cat_wise"> Category wise');
    else
        $('#domain_cont_type').html('<input type="radio" name="dom_cont_type" value="none" checked> None <input type="radio" name="dom_cont_type" value="dom_wise"> Domain wise <input type="radio" name="dom_cont_type" value="cat_wise"> Category wise');
    $(this).parent().html('<a id="save_vi" style="cursor: pointer">Save</a>');
}

function wmpEdit() {
    if ($('#domain_dataSource').text() === 'seed')
        $('#domain_dataSource').html('<input type="radio" name="seed_api" value="seed" checked> Seed <input type="radio" name="seed_api" value="api"> API');
    else
        $('#domain_dataSource').html('<input type="radio" name="seed_api" value="seed"> Seed <input type="radio" name="seed_api" value="api" checked> API');
    if ($('#domain_WMPStatus').text() === 'active')
        $('#domain_WMPStatus').html('<input type="radio" name="wmp_status" value="active" checked> Active <input type="radio" name="wmp_status" value="inactive"> Inactive');
    else
        $('#domain_WMPStatus').html('<input type="radio" name="wmp_status" value="active"> Active <input type="radio" name="wmp_status" value="inactive" checked> Inactive');
    $('#domain_push_percentage').html("<input id='dom_push_percent' data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $('#domain_push_percentage').text() + "'/>");
    $(this).parent().html('<a id="save_wmp" style="cursor: pointer">Save</a>');
}

function viSave() {
    var vi_obj = {};
    var dom_name = $('#domain_name').text();
    var temp = "";
    var selected = $("input[type='radio'][name='status']:checked");
    if (selected.length > 0) {
        temp = selected.val();
    }
    $('#domain_status').text(temp);
    vi_obj["status."+$('#selected_isp').val()] = temp;
    temp = parseInt($('#dom_capacity').val());
    $('#domain_capacity').text(temp);
    vi_obj["perday_capacity."+$('#selected_isp').val()] = temp;
    temp = parseInt($('#dom_start_hour').val());
    $('#domain_startHour').text(temp);
    vi_obj["start_hour."+$('#selected_isp').val()] = temp;
    temp = parseInt($('#dom_api_port').val());
    $('#domain_api_port').text(temp);
    vi_obj["mtastats_api_port"] = temp;
    selected = $("input[type='radio'][name='dom_cont_type']:checked");
    if (selected.length > 0) {
        temp = selected.val();
    }
    $('#domain_cont_type').text(temp);
    vi_obj["cnt_type."+$('#selected_isp').val()] = temp;

    console.log({"vi_obj": vi_obj, "domain_name": dom_name});
    $.post("/domain_settings/saveDoc/"+dom_name+"/"+$('#selected_isp').val(), vi_obj, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
    $(this).parent().html('<a id="edit_vi" style="cursor: pointer">Edit</a>');
}

function wmpSave() {
    var vi_obj = {};
    var dom_name = $('#domain_name').text();
    var temp = "";
    var selected = $("input[type='radio'][name='seed_api']:checked");
    if (selected.length > 0) {
        temp = selected.val();
    }
    $('#domain_dataSource').text(temp);
    vi_obj["wmp_data_input."+$('#selected_isp').val()] = temp;
    temp = "";
    var selected = $("input[type='radio'][name='wmp_status']:checked");
    if (selected.length > 0) {
        temp = selected.val();
    }
    $('#domain_WMPStatus').text(temp);
    vi_obj["wmp_status."+$('#selected_isp').val()] = temp;
    temp = $('#dom_push_percent').val();
    $('#domain_push_percentage').text(temp);
    vi_obj["wmp_push_percent."+$('#selected_isp').val()] = temp;

    console.log({"vi_obj": vi_obj, "domain_name": dom_name});
    $.post("/domain_settings/saveDoc2/"+dom_name+"/"+$('#selected_isp').val(), vi_obj, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
    $(this).parent().html('<a id="edit_wmp" style="cursor: pointer">Edit</a>');
}

function fillDomains() {
    $.getJSON('/domains/domainList', function (data) {
        $('#domain_wise').html('');
        $.each(data, function () {
            $('#domain_wise').append($('<option>', {value: this.domain_name, text: this.domain_name}));
        });
        populateSettingsTable();
    });
}

function populateSettingsTable() {
    var tableContent = '';
    var capacity;
    var status;
    $('#domain_name').text('');
    $('#domain_status').text('');
    $('#domain_capacity').text('');
    $('#domain_startHour').text('');
    $('#domain_dataSource').text('');
    $('#domain_WMPStatus').text('');
    $('#domain_push_percentage').text('');
    $('#domain_api_port').text('');
    $('#domain_cont_type').text('');
    $.getJSON('/domain_settings/getDomain/'+$('#domain_wise').val(), function (data) {
        console.log(data);
        $('#domain_name').text(data.domain_name);
        $('#domain_status').text(data.status[$('#selected_isp').val()]);
        $('#domain_capacity').text(data.perday_capacity[$('#selected_isp').val()]);
        $('#domain_startHour').text(data.start_hour[$('#selected_isp').val()]);
        $('#domain_dataSource').text((data.wmp_data_input && data.wmp_data_input[$('#selected_isp').val()])?data.wmp_data_input[$('#selected_isp').val()]:"");
        $('#domain_WMPStatus').text((data.wmp_status && data.wmp_status[$('#selected_isp').val()])?data.wmp_status[$('#selected_isp').val()]:"");
        $('#domain_push_percentage').text((data.wmp_push_percent && data.wmp_push_percent[$('#selected_isp').val()])?data.wmp_push_percent[$('#selected_isp').val()]:"");
        $('#domain_cont_type').text((data.cnt_type && data.cnt_type[$('#selected_isp').val()])?data.cnt_type[$('#selected_isp').val()]:"");
        $('#domain_api_port').text(data.mtastats_api_port?data.mtastats_api_port:"");
    });
    $.getJSON('/domain_settings/domainList', function (data) {
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td rowspan="3"><b><i>' + this.domain_name + '</i></b></td>';
            tableContent += '<td>status</td>';
            if (typeof this.status === 'object') {
                status = this.status;

                tableContent += '<td>' + status.google + '</td>';
                tableContent += '<td>' + status.yahoodns + '</td>';
                tableContent += '<td>' + status.aol + '</td>';
                tableContent += '<td>' + status.hotmail + '</td>';
                tableContent += '<td>' + status.others + '</td>';
            } else {
                tableContent += '<td>inactive</td>';
                tableContent += '<td>inactive</td>';
                tableContent += '<td>inactive</td>';
                tableContent += '<td>inactive</td>';
                tableContent += '<td>inactive</td>';
            }
            tableContent += '<td><a href="#" class="linkedituserStatus" rel="' + this._id + '">edit</a></td>';
            tableContent += '</tr><tr><td>capacity /day</td>';
            if (typeof this.perday_capacity === 'object') {
                capacity = this.perday_capacity;

                tableContent += '<td>' + capacity.google + '</td>';
                tableContent += '<td>' + capacity.yahoodns + '</td>';
                tableContent += '<td>' + capacity.aol + '</td>';
                tableContent += '<td>' + capacity.hotmail + '</td>';
                tableContent += '<td>' + capacity.others + '</td>';
            } else {
                tableContent += '<td>0</td>';
                tableContent += '<td>0</td>';
                tableContent += '<td>0</td>';
                tableContent += '<td>0</td>';
                tableContent += '<td>0</td>';
            }
            tableContent += '<td><a href="#" class="linkedituserCapacity" rel="' + this._id + '">edit</a></td>';
            tableContent += '</tr><tr><td>start hour</td>';
            if (typeof this.start_hour === 'object') {
                start_hour = this.start_hour;

                tableContent += '<td>' + start_hour.google + '</td>';
                tableContent += '<td>' + start_hour.yahoodns + '</td>';
                tableContent += '<td>' + start_hour.aol + '</td>';
                tableContent += '<td>' + start_hour.hotmail + '</td>';
                tableContent += '<td>' + start_hour.others + '</td>';
            } else {
                tableContent += '<td>24</td>';
                tableContent += '<td>24</td>';
                tableContent += '<td>24</td>';
                tableContent += '<td>24</td>';
                tableContent += '<td>24</td>';
            }
            tableContent += '<td><a href="#" class="linkedituserStartHour" rel="' + this._id + '">edit</a></td>';
            tableContent += '</tr>';
            //tableContent += '<td>' + this.api_key + '</td>';
        });
        $('#userList table tbody').html(tableContent);
    });
}

function linkEditDomainStatus(event) {
    event.preventDefault();
    var i = 0;
    $(this).parent().parent().children('td').each(function () {
        //alert($(this).html());
        switch (i) {
            case 2 :
                //$(this).html("<input style='width: 22px;' type='text' value='"+$(this).text()+"'/>");
                if ($(this).text() === 'active')
                    $(this).html("<select><option value='active' selected>active</option><option value='inactive'>inactive</option></select>");
                else
                    $(this).html("<select><option value='inactive' selected>inactive</option><option value='active'>active</option></select>");
                break;
            case 3 :
                if ($(this).text() === 'active')
                    $(this).html("<select><option value='active' selected>active</option><option value='inactive'>inactive</option></select>");
                else
                    $(this).html("<select><option value='inactive' selected>inactive</option><option value='active'>active</option></select>");
                break;
            case 4 :
                if ($(this).text() === 'active')
                    $(this).html("<select><option value='active' selected>active</option><option value='inactive'>inactive</option></select>");
                else
                    $(this).html("<select><option value='inactive' selected>inactive</option><option value='active'>active</option></select>");
                break;
            case 5 :
                if ($(this).text() === 'active')
                    $(this).html("<select><option value='active' selected>active</option><option value='inactive'>inactive</option></select>");
                else
                    $(this).html("<select><option value='inactive' selected>inactive</option><option value='active'>active</option></select>");
                break;
            case 6 :
                if ($(this).text() === 'active')
                    $(this).html("<select><option value='active' selected>active</option><option value='inactive'>inactive</option></select>");
                else
                    $(this).html("<select><option value='inactive' selected>inactive</option><option value='active'>active</option></select>");
                break;
            case 7 :
                $(this).html("<a href='#' style='color: green;' class='linksaveuserStatus' rel=" + $(this).children('a').attr('rel') + ">save</a>");
                break;
        }
        i++;
    });
}

function linkEditDomainStartHour(event) {
    event.preventDefault();
    var i = 0;
    $(this).parent().parent().children('td').each(function () {
        switch (i) {
            case 1 :
                $(this).html("<input data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 2 :
                $(this).html("<input data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 3 :
                $(this).html("<input data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 4 :
                $(this).html("<input data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 5 :
                $(this).html("<input data-time-format='H:i' class='timePicker time' style='width: 75px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 6 :
                $(this).html("<a href='#' style='color: green;' class='linksaveuserStartHour' rel=" + $(this).children('a').attr('rel') + ">save</a>");
                break;
        }
        i++;
    });
    $(function() {
        $('.timePicker').timepicker({ 'scrollDefault': 'now', 'step': 60});
    });
}

function linkEditDomainCapacity(event) {
    event.preventDefault();
    var i = 0;
    $(this).parent().parent().children('td').each(function () {
        //alert($(this).html());
        switch (i) {
            case 1 :
                $(this).html("<input style='width: 100px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 2 :
                $(this).html("<input style='width: 100px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 3 :
                $(this).html("<input style='width: 100px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 4 :
                $(this).html("<input style='width: 100px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 5 :
                $(this).html("<input style='width: 100px;' type='text' value='" + $(this).text() + "'/>");
                break;
            case 6 :
                $(this).html("<a href='#' style='color: green;' class='linksaveuserCapacity' rel=" + $(this).children('a').attr('rel') + ">save</a>");
                break;
        }
        i++;
    });
}

function linkSaveDomainStatus(event) {
    event.preventDefault();
    var i = 0;
    var status = {};
    var temp;
    $(this).parent().parent().children('td').each(function () {
        switch (i) {
            case 2 :
                temp = $(this).children('select').val().trim();
                if (temp === 'active')
                    $(this).html("active");
                else
                    $(this).html("inactive");
                status['google'] = temp;
                break;
            case 3 :
                temp = $(this).children('select').val().trim();
                if (temp === 'active')
                    $(this).html("active");
                else
                    $(this).html("inactive");
                status['yahoodns'] = temp;
                break;
            case 4 :
                temp = $(this).children('select').val().trim();
                if (temp === 'active')
                    $(this).html("active");
                else
                    $(this).html("inactive");
                status['aol'] = temp;
                break;
            case 5 :
                temp = $(this).children('select').val().trim();
                if (temp === 'active')
                    $(this).html("active");
                else
                    $(this).html("inactive");
                status['hotmail'] = temp;
                break;
            case 6 :
                temp = $(this).children('select').val().trim();
                if (temp === 'active')
                    $(this).html("active");
                else
                    $(this).html("inactive");
                status['others'] = temp;
                break;
            case 7 :
                $(this).html("<a href='#' class='linkedituserStatus' rel='" + $(this).children('a').attr('rel') + "'>edit</a>");
                break;
        }
        i++;
    });
    $.post("/domain_settings/updatedomainStatus/" + $(this).attr('rel'), status, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
}

function linkSaveDomainCapacity(event) {
    event.preventDefault();
    var i = 0;
    var capacity = {};
    var temp;
    $(this).parent().parent().children('td').each(function () {
        switch (i) {
            case 1 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                capacity['google'] = temp;
                break;
            case 2 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                capacity['yahoodns'] = temp;
                break;
            case 3 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                capacity['aol'] = temp;
                break;
            case 4 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                capacity['hotmail'] = temp;
                break;
            case 5 :
                temp = $(this).children('input').val().trim();
                $(this).html(temp);
                capacity['others'] = temp;
                break;
            case 6 :
                $(this).html("<a href='#' class='linkedituserCapacity' rel='" + $(this).children('a').attr('rel') + "'>edit</a>");
                break;
        }
        i++;
    });
    $.post("/domain_settings/updatedomainCapacity/" + $(this).attr('rel'), capacity, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
}

function linkSaveDomainStartHour(event) {
    event.preventDefault();
    var i = 0;
    var start_hour = {};
    var temp;
    $(this).parent().parent().children('td').each(function () {
        switch (i) {
            case 1 :
                temp = parseInt($(this).children('input').val().trim().split(':')[0]);
                $(this).html(temp);
                start_hour['google'] = temp;
                break;
            case 2 :
                temp = parseInt($(this).children('input').val().trim().split(':')[0]);
                $(this).html(temp);
                start_hour['yahoodns'] = temp;
                break;
            case 3 :
                temp = parseInt($(this).children('input').val().trim().split(':')[0]);
                $(this).html(temp);
                start_hour['aol'] = temp;
                break;
            case 4 :
                temp = parseInt($(this).children('input').val().trim().split(':')[0]);
                $(this).html(temp);
                start_hour['hotmail'] = temp;
                break;
            case 5 :
                temp = parseInt($(this).children('input').val().trim().split(':')[0]);
                $(this).html(temp);
                start_hour['others'] = temp;
                break;
            case 6 :
                $(this).html("<a href='#' class='linkedituserStartHour' rel='" + $(this).children('a').attr('rel') + "'>edit</a>");
                break;
        }
        i++;
    });
    $.post("/domain_settings/updatedomainStartHour/" + $(this).attr('rel'), start_hour, function (response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
}