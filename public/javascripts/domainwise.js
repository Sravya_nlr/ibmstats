$(document).ready(function () {
    $('#domainwise_stats').addClass('active');
    $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", new Date());
    fillDomains();
    populateTable();
    $('#datepicker').on('change', function () {
        populateTable();
    });
    $('#domain_wise').on('change', function () {
        populateTable();
    });
    $('#datepicker').bind("enterKey", function (e) {
        populateTable();
    });
    $('#datepicker').keyup(function (e) {
        if (e.keyCode == 13)
            $(this).trigger("enterKey");
    });
    $('#refresh').on('click', function () {
        populateTable();
    });
});

function fillDomains() {
    $.getJSON('/domains/domainList', function (data) {
        $.each(data, function () {
            $('#domain_wise').append($('<option>', {value: this.domain_name, text: this.domain_name}));
        });
    });
}

function populateTable() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    $('#refresh').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/domainwise/getStats/' + $('#domain_wise').val() + '/' + $('#datepicker').val(), function (data) {
        var i = 0;
        var temp;
        var google;
        var yahoodns;
        var aol;
        var hotmail;
        var others;
        var total;
        if (data.length !== 0) {
            temp = data[i];
            $('option').each(function () {
                $(this).removeClass('deferred');
            });
            if (temp.length > 0) {
                for (var a in temp) {
                    $('option:contains(' + temp[a] + ')').addClass('deferred');
                }
            }
            i++;
            if (data[i] !== undefined) {
                temp = data[i].isp_wise;
                if (typeof temp !== 'undefined') {
                    google = temp.google;
                    yahoodns = temp.yahoodns;
                    aol = temp.aol;
                    hotmail = temp.hotmail;
                    others = temp.others;
                }
                total = data[i].total;

                tableContent += '<tr>';
                tableContent += '<td><strong>income</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.income === 'undefined') ? 0 : google.income) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.income === 'undefined') ? 0 : yahoodns.income) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.income === 'undefined') ? 0 : aol.income) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.income === 'undefined') ? 0 : hotmail.income) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.income === 'undefined') ? 0 : others.income) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.income === 'undefined') ? 0 : total.income) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>processed</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.processed === 'undefined') ? 0 : google.processed) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.processed === 'undefined') ? 0 : yahoodns.processed) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.processed === 'undefined') ? 0 : aol.processed) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.processed === 'undefined') ? 0 : hotmail.processed) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.processed === 'undefined') ? 0 : others.processed) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.processed === 'undefined') ? 0 : total.processed) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>sent</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.sent === 'undefined') ? 0 : google.sent) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.sent === 'undefined') ? 0 : yahoodns.sent) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.sent === 'undefined') ? 0 : aol.sent) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.sent === 'undefined') ? 0 : hotmail.sent) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.sent === 'undefined') ? 0 : others.sent) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.sent === 'undefined') ? 0 : total.sent) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>bounced</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.bounced === 'undefined') ? 0 : google.bounced) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.bounced === 'undefined') ? 0 : yahoodns.bounced) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.bounced === 'undefined') ? 0 : aol.bounced) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.bounced === 'undefined') ? 0 : hotmail.bounced) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.bounced === 'undefined') ? 0 : others.bounced) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.bounced === 'undefined') ? 0 : total.bounced) + '</td>';
                tableContent += '</tr>';

                tableContent += '<tr>';
                tableContent += '<td><strong>deferred</strong></td>';
                tableContent += '<td>' + ((typeof google === 'undefined' || typeof google.defered === 'undefined') ? 0 : google.defered) + '</td>';
                tableContent += '<td>' + ((typeof yahoodns === 'undefined' || typeof yahoodns.defered === 'undefined') ? 0 : yahoodns.defered) + '</td>';
                tableContent += '<td>' + ((typeof aol === 'undefined' || typeof aol.defered === 'undefined') ? 0 : aol.defered) + '</td>';
                tableContent += '<td>' + ((typeof hotmail === 'undefined' || typeof hotmail.defered === 'undefined') ? 0 : hotmail.defered) + '</td>';
                tableContent += '<td>' + ((typeof others === 'undefined' || typeof others.defered === 'undefined') ? 0 : others.defered) + '</td>';
                tableContent += '<td>' + ((typeof total === 'undefined' || typeof total.defered === 'undefined') ? 0 : total.defered) + '</td>';
                tableContent += '</tr>';
            } else {
                tableContent += '<tr><td colspan="7">No Records found!</tr>';
            }
        }
        $('#userList table tbody').html(tableContent);
        var image = new Image();
        image.src = 'images/icon-refresh.png';
        $('#refresh').attr('src', image.src);
    });
}
