/**
 * Created by sys050 on 30/3/17.
 */

var TMData = [];
$('#refresh2').on('click', function () {
    populateTable();
});
$('#refresh3').on('click', function () {
    populateTable2();
});
/*$('#refresh4').on('click', function () {
    populateTable3();
});
$('#refresh5').on('click', function () {
    populateTable4();
});
$('#refresh6').on('click', function () {
    populateTable5();
});*/
$(document).ready(function () {
    $('#fb_valid').addClass('active');
    populateTable();
    populateTable2();
   /* populateTable3();
    populateTable4();
    populateTable5();*/

});

function populateTable() {
    var image = new Image();
    // image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    image.src = 'https://media.giphy.com/media/IB9foBA4PVkKA/giphy.gif';

    $('#refresh2').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/daywise_count/daywise_count_data', function (data) {
        TMData = data;
        image.src = 'images/icon-refresh.png';
        $('#refresh2').attr('src', image.src);

        // console.log("dataaaa",data);
        $.each(data,function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.Date+'</td>';
            tableContent += '<td>' + this.Gcount+'</td>';
            tableContent += '</tr>';
        });
        $('#userList table tbody').html(tableContent);
    });
}
function populateTable2() {
    var image = new Image();
    // image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    image.src = 'https://media.giphy.com/media/IB9foBA4PVkKA/giphy.gif';
    $('#refresh3').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/daywise_count/daywise_count_Ydata', function (data) {
        TMData = data;
        image.src = 'images/icon-refresh.png';
        $('#refresh3').attr('src', image.src);

        // console.log("dataaaa",data);
        $.each(data,function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.Date+'</td>';
            tableContent += '<td>' + this.Ycount+'</td>';
            tableContent += '</tr>';
        });
        $('#userList2 table tbody').html(tableContent);
    });
}
/*function populateTable3() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';

    $('#refresh4').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/fb_valid/fb_valid_data3', function (data) {
        TMData = data;
        image.src = 'images/icon-refresh.png';
        $('#refresh4').attr('src', image.src);
        console.log("dataaaa",data);
        tableContent += '<tr>';
        tableContent += '<td>' + data.count+'</td>';
        tableContent += '</tr>';
        $('#userList3 table tbody').html(tableContent);
    });
}
function populateTable4() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';

    $('#refresh5').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/fb_valid/fb_valid_data4', function (data) {
        image.src = 'images/icon-refresh.png';
        $('#refresh5').attr('src', image.src);
        TMData = data;
        console.log("dataaaa",data);
        tableContent += '<tr>';
        tableContent += '<td>' + data.count+'</td>';
        tableContent += '</tr>';
        $('#userList4 table tbody').html(tableContent);
    });
}
function populateTable5() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';

    $('#refresh6').attr('src', image.src);

    var tableContent = '';
    $.getJSON('/fb_valid/fb_valid_data5', function (data) {
        image.src = 'images/icon-refresh.png';
        $('#refresh6').attr('src', image.src);
        TMData = data;
        console.log("dataaaa",data);
        tableContent += '<tr>';
        tableContent += '<td>' + data.count+'</td>';
        tableContent += '</tr>';
        $('#userList5 table tbody').html(tableContent);
    });
}*/
/*

// Add Domain
function addDomain(event) {
    event.preventDefault();

    var errorCount = 0;
    $('#addDomain input').each(function (index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    if (errorCount === 0) {
        var newDomain = {
            'domain_name': $('#addDomain fieldset input#inputDomainDomainName').val().trim(),
            'domain_ip': $('#addDomain fieldset input#inputDomainDomainIP').val().trim(),
            'gmail_cap': $('#addDomain fieldset input#inputDomainGoogleCapacity').val().trim(),
            'yahoo_cap': $('#addDomain fieldset input#inputDomainYahooCapacity').val().trim(),
            'hotmail_cap': $('#addDomain fieldset input#inputDomainHotmailCapacity').val().trim(),
            'aol_cap': $('#addDomain fieldset input#inputDomainAOLCapacity').val().trim(),
            'others_cap': $('#addDomain fieldset input#inputDomainOthersCapacity').val().trim(),
            'api_port': $('#addDomain fieldset input#inputDomainAPIPort').val().trim()
        };
        $.post("/domains/addDomain", newDomain, function (response) {
            if (response.msg === '') {
                $('#addDomain fieldset input').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
}

// Delete Domain
function deleteDomain(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/domains/deleteDomain/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            populateTable();
        });
    }
    else {
        return false;
    }
}*/
