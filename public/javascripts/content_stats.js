$(document).ready(function () {
    $('#content_stats').addClass('active');
    $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", new Date());
    $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", new Date());
    fillDomains();
    populateTable();
    populateTable2();
    $('#datepicker').on('change', function () {
        populateTable();
    });
    $('#datepicker2').on('change', function () {
        populateTable2();
    });
    $('#domain_id, #category_id, #esp_id').on('change', function () {
        populateTable();
    });
    $('#domain_id2, #esp_id2').on('change', function () {
        populateTable2();
    });
    $('#email_id2').bind("enterKey", function (e) {
        populateTable2();
    });
    $('#datepicker, #email_id').bind("enterKey", function (e) {
        populateTable();
    });
    $('#datepicker, #email_id, #email_id2').keyup(function (e) {
        if (e.keyCode == 13)
            $(this).trigger("enterKey");
    });
    $('#refresh').on('click', function () {
        populateTable();
    });
    $('#refresh2').on('click', function () {
        populateTable2();
    });
});

function fillDomains() {
    $.getJSON('/domains/domainList', function (data) {
        $.each(data, function () {
            $('#domain_id').append($('<option>', {value: this._id, text: this.domain_name}));
            $('#domain_id2').append($('<option>', {value: this._id, text: this.domain_name}));
        });
    });
    //$.getJSON('/users/userlist', function (data) {
    //    $.each(data, function () {
    //        $('#user_id').append($('<option>', {value: this._id, text: this.email}));
    //    });
    //});
    $.getJSON('/content_details/contentlist', function (data) {
        $.each(data, function () {
            $('#category_id').append($('<option>', {value: this._id, text: this.category}));
        });
    });
}

function populateTable() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    $('#refresh').attr('src', image.src);

    var tableContent = '';
    var email_id = ($('#email_id').val()==''?'none':$('#email_id').val());
    $.getJSON('/content_stats/getStats/' + $('#domain_id').val() + '/' + $('#esp_id').val() + '/' + $('#category_id').val() + '/' + email_id + '/' + $('#datepicker').val(), function (data) {
        console.log(typeof data, data);
        if(JSON.stringify(data)==='{}') {
            tableContent += '<tr><td colspan="9">No Records found!</tr>';
        } else {
            tableContent += '<tr>';
            tableContent += '<td>' + data.s + '</td>';
            tableContent += '<td>' + data.b + '</td>';
            tableContent += '<td>' + data.d + '</td>';
            tableContent += '<td>' + data.o + '</td>';
            tableContent += '<td>' + data.uo + '</td>';
            tableContent += '<td>' + data.c + '</td>';
            tableContent += '<td>' + data.uc + '</td>';
            tableContent += '<td>' + data.u + '</td>';
            tableContent += '<td>' + data.uu + '</td>';
            tableContent += '</tr>';
        }

        $('#userList table tbody').html(tableContent);
        var image = new Image();
        image.src = 'images/icon-refresh.png';
        $('#refresh').attr('src', image.src);
    });
}


function populateTable2() {
    var image = new Image();
    image.src = 'http://rack.3.mshcdn.com/media/ZgkyMDEyLzEwLzE5LzExXzMzXzMzXzE3Nl9maWxlCnAJdGh1bWIJMTIwMHg5NjAwPg/462b8072';
    $('#refresh2').attr('src', image.src);

    var tableContent = '';
    var email_id = ($('#email_id2').val()==''?'none':$('#email_id2').val());
    $.getJSON('/content_stats/getStats2/' + $('#esp_id2').val() + '/' + $('#domain_id2').val() + '/' + email_id + '/' + $('#datepicker2').val(), function (data) {
        console.log(typeof data, data);
        if(JSON.stringify(data)==='{}') {
            tableContent += '<tr><td colspan="9">No Records found!</tr>';
        } else {
            tableContent += '<tr>';
            tableContent += '<td>' + data.s + '</td>';
            tableContent += '<td>' + data.b + '</td>';
            tableContent += '<td>' + data.d + '</td>';
            tableContent += '<td>' + data.o + '</td>';
            tableContent += '<td>' + data.uo + '</td>';
            tableContent += '<td>' + data.c + '</td>';
            tableContent += '<td>' + data.uc + '</td>';
            tableContent += '<td>' + data.u + '</td>';
            tableContent += '<td>' + data.uu + '</td>';
            tableContent += '</tr>';
        }

        $('#userList2 table tbody').html(tableContent);
        var image = new Image();
        image.src = 'images/icon-refresh.png';
        $('#refresh2').attr('src', image.src);
    });
}
