var userListData = [];

$(document).ready(function () {
    $('#list_upload').addClass('active');
    //populateTable();
    // Add User button click
    $('#btnAddContent').on('click', addContent);
    // Delete User link click
    $('#userList table tbody').on('click', 'td a.linkdeletecontent', deleteConent);
});

function populateTable() {
    var tableContent = '';
    $.getJSON('/content_details/contentlist', function (data) {
        userListData = data;
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td><a href="#" class="linkshowcontent" rel="' + this.category + '">' + this.category + '</a></td>';
            tableContent += '<td><a href="#" class="linkdeletecontent" rel="' + this._id + '">delete</a></td>';
            tableContent += '</tr>';
        });

        $('#userList table tbody').html(tableContent);
        $('#userList table tbody').on('click', 'td a.linkshowcontent', showContentInfo);
    });
}

// Show Content Info
function showContentInfo(event) {
    event.preventDefault();
    var thisContentName = $(this).attr('rel');
    var arrayPosition = userListData.map(function (arrayItem) {
        return arrayItem.category;
    }).indexOf(thisContentName);
    var thisUserObject = userListData[arrayPosition];

    $('#categoryId').text(thisUserObject._id);
    $('#category').text(thisUserObject.category);
    $('#subject').text(thisUserObject.subject);
    $('#content').text(thisUserObject.content);
}

// Add Content
function addContent(event) {
    event.preventDefault();

    if ($('#inputCategoryId').val() === '' || $('#inputCategory').val() === '' || $('#inputSubject').val() === '' || $('#inputContent').val() === '') {
        alert('Please fill in all fields');
        return false;
    } else {
        var newContent = {
            '_id': $('#inputCategoryId').val().trim(),
            'category': $('#inputCategory').val().trim(),
            'subject': $('#inputSubject').val().trim(),
            'content': $('#inputContent').val().trim().replace(/>\s+</g, "><")
        };
        $.post("/content_details/addcontent", newContent, function (response) {
            if (response.msg === '') {
                $('#inputCategory').val('');
                $('#inputSubject').val('');
                $('#inputCategoryId').val('');
                $('#inputContent').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
}

// Delete Content
function deleteConent(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this category?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/content_details/deletecontent/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            populateTable();
        });
    }
    else {
        return false;
    }
}