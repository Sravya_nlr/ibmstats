var userListData = [];

$(document).ready(function () {
    $('#user_details').addClass('active');
    populateTable();
    // Add User button click
    $('#btnAddUser').on('click', addUser);
    // Delete User link click
    $('#userList table tbody').on('click', 'td a.linkdeleteuser', deleteUser);
});

function populateTable() {
    var tableContent = '';
    $.getJSON('/users/userlist', function (data) {
        userListData = data;
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td><a href="#" class="linkshowuser" rel="' + this.email + '">' + this.name + '</a></td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td><a href="#" class="linkdeleteuser" rel="' + this._id + '">delete</a></td>';
            tableContent += '</tr>';
        });

        $('#userList table tbody').html(tableContent);
        $('#userList table tbody').on('click', 'td a.linkshowuser', showUserInfo);
    });
}


// Show User Info
function showUserInfo(event) {
    event.preventDefault();
    var thisUserName = $(this).attr('rel');
    var arrayPosition = userListData.map(function (arrayItem) {
        return arrayItem.email;
    }).indexOf(thisUserName);
    var thisUserObject = userListData[arrayPosition];

    $('#userInfoName').text(thisUserObject.name);
    $('#userInfoAge').text(thisUserObject.age);
    $('#userInfoGender').text(thisUserObject.gender);
    $('#userInfoLocation').text(thisUserObject.location);
    $('#userInfoAPIKey').text(thisUserObject.api_key);
}

function capitalize(str, all) {
    var lastResponded;
    return str.toLowerCase().replace(all ? /[^']/g : /^\S/, function (lower) {
        var upper = lower.toUpperCase(),
            result;
        result = lastResponded ? lower : upper;
        lastResponded = upper !== lower;
        return result;
    });
}

// Add User
function addUser(event) {
    event.preventDefault();

    var errorCount = 0;
    $('#addUser input').each(function (index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    if (errorCount === 0) {
        var newUser = {
            'name': capitalize($('#addUser fieldset input#inputUserFirstname').val().trim() + ' ' + $('#addUser fieldset input#inputUserLastname').val().trim(), true),
            'email': $('#addUser fieldset input#inputUserEmail').val().trim(),
            'age': $('#addUser fieldset input#inputUserAge').val().trim(),
            'location': capitalize($('#addUser fieldset input#inputUserLocation').val().trim(), true),
            'gender': $('#addUser fieldset select#inputUserGender').val(),
            'username': $('#addUser fieldset input#inputUserEmail').val().trim(),
            'priority': 0
        };
        $.post("/users/adduser", newUser, function (response) {
            if (response.msg === '') {
                $('#addUser fieldset input').val('');
                populateTable();
            }
            else {
                alert('Error: ' + response.msg);
            }
        });
    }
    else {
        alert('Please fill in all fields');
        return false;
    }
}

// Delete User
function deleteUser(event) {

    event.preventDefault();
    var confirmation = confirm('Are you sure you want to delete this user?');
    if (confirmation === true) {
        $.ajax({
            type: 'DELETE',
            url: '/users/deleteuser/' + $(this).attr('rel')
        }).done(function (response) {
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }
            populateTable();
        });
    }
    else {
        return false;
    }
}